package aoc

import "testing"

func TestDay02Example1(t *testing.T) {
	data := []string{"1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"}
	answer := day02solve1(data, 3)

	if answer != 2 {
		t.Errorf("Day02Example1 Answer found was %d, expected was 2", answer)
	}
}

func TestDay02Part1(t *testing.T) {
	data, count := loadData("../../../data/day02.dat")

	answer := day02solve1(data, count)

	if answer != 636 {
		t.Errorf("Day02Part1: Answer found was %d, expected was 636", answer)
	}
}

func TestDay02Example2(t *testing.T) {
	data := []string{"1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"}
	answer := day02solve2(data, 3)

	if answer != 1 {
		t.Errorf("Day02Example2 Answer found was %d, expected was 1", answer)
	}
}

func TestDay02Part2(t *testing.T) {
	data, count := loadData("../../../data/day02.dat")

	answer := day02solve2(data, count)

	if answer != 588 {
		t.Errorf("Day02Part2: Answer found was %d, expected was 588", answer)
	}
}
