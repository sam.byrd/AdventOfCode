package aoc

import "testing"

func TestDay05Example1(t *testing.T) {

	data := []string{"FBFBBFFRLR"}

	answer := day05part1(data, len(data))

	if answer != 357 {
		t.Errorf("TestDay05Example1 gave %d, expected 357", answer)
	}
}

func TestDay05Part1(t *testing.T) {
	data, count := loadData("../../../data/day05.dat")

	answer := day05part1(data, count)

	if answer != 842 {
		t.Errorf("Day05Part1 returned %d, expected 842", answer)
	}
}

func TestDay05Part2(t *testing.T) {
	data, count := loadData("../../../data/day05.dat")

	answer := day05part2(data, count)

	if answer != 617 {
		t.Errorf("Day05Part2 returned %d, expected 617", answer)
	}
}
