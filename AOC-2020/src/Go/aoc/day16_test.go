package aoc

import (
	"testing"
)

func TestDay16Part1Example1(t *testing.T) {
	data := []string{
		"class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"seat: 13-40 or 45-50",
		"",
		"your ticket:",
		"7,1,14",
		"",
		"nearby tickets:",
		"7,3,47",
		"40,4,50",
		"55,2,20",
		"38,6,12",
	}

	answer := Day16Part1(data)

	if answer != 71 {
		t.Errorf("Testday16Part1Example1 gave %d, expected 71", answer)
	}
}

func TestDay16Part1(t *testing.T) {

	data, _ := loadData("../../../data/day16.dat")

	answer := Day16Part1(data)

	if answer != 21980 {
		t.Errorf("TestDay16Part1 returned %d, expected 21980", answer)
	}
}

func TestDay16Part2Example1(t *testing.T) {
	data := []string{
		"departure class: 1-3 or 5-7",
		"row: 6-11 or 33-44",
		"departure seat: 13-40 or 45-50",
		"",
		"your ticket:",
		"7,1,14",
		"",
		"nearby tickets:",
		"7,3,47",
		"40,4,50",
		"55,2,20",
		"38,6,12",
	}

	answer := Day16Part2(data)

	if answer != 1 {
		t.Errorf("Testday16Part1Example1 gave %d, expected 1", answer)
	}
}

func TestDay16Part2Example2(t *testing.T) {
	data := []string{
		"departure location: 29-766 or 786-950",
		"departure station: 40-480 or 491-949",
		"departure platform: 46-373 or 397-957",
		"departure track: 33-657 or 673-970",
		"departure date: 31-433 or 445-961",
		"departure time: 33-231 or 250-966",
		"arrival location: 48-533 or 556-974",
		"arrival station: 42-597 or 620-957",
		"arrival platform: 32-119 or 140-967",
		"arrival track: 28-750 or 762-973",
		"class: 26-88 or 101-950",
		"duration: 30-271 or 293-974",
		"price: 33-712 or 718-966",
		"route: 49-153 or 159-953",
		"row: 36-842 or 851-972",
		"seat: 43-181 or 194-955",
		"train: 29-500 or 513-964",
		"type: 32-59 or 73-974",
		"wagon: 47-809 or 816-957",
		"zone: 44-451 or 464-955",
		"",
		"your ticket:",
		"151,103,173,199,211,107,167,59,113,179,53,197,83,163,101,149,109,79,181,73",
		"",
		"nearby tickets:",
		"991,521,595,300,422,471,686,620,159,341,448,730,694,513,342,575,743,213,790,684",
		"775,312,631,797,931,103,109,588,109,464,680,293,655,149,167,821,415,816,260,881",
		"0,809,569,206,625,431,56,448,913,800,476,344,882,269,166,314,908,202,373,570",
		"988,891,824,632,229,909,571,176,928,586,708,117,471,560,864,805,725,161,114,934",
	}

	answer := Day16Part2(data)

	if answer != 1 {
		t.Errorf("Testday16Part1Example2 gave %d, expected 1", answer)
	}
}

func TestDay16Part2(t *testing.T) {

	data, _ := loadData("../../../data/day16.dat")

	answer := Day16Part2(data)

	if answer != 1439429522627 {
		t.Errorf("TestDay16Part2 returned %d, expected 1439429522627", answer)
	}
}

func TestDay16Part2Eli(t *testing.T) {

	data, _ := loadData("../../../data/day16_eli.dat")

	answer := Day16Part2(data)

	if answer != 2843534243843 {
		t.Errorf("TestDay16Part2 returned %d, expected 2843534243843", answer)
	}
}

func TestDay16Part2Alt(t *testing.T) {
	Day16Alt()
}
