package aoc

import "testing"

func TestDay12Part1Example1(t *testing.T) {
	data := []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}

	answer := day12Part1(data)

	if answer != float64(25) {
		t.Errorf("Testday12Example1 gave %f, expected 25", answer)
	}
}

func TestDay12Part2Example1(t *testing.T) {
	data := []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}

	answer := day12Part2(data)

	if answer != 286 {
		t.Errorf("Testday12Part2Example1 gave %f, expected 286", answer)
	}
}

func TestDay12Part1(t *testing.T) {

	data, _ := loadData("../../../data/day12.dat")

	answer := day12Part1(data)

	if answer != float64(882) {
		t.Errorf("Test08Part1 returned %f, expected 882", answer)
	}
}

func TestDay12Part2(t *testing.T) {

	data, _ := loadData("../../../data/day12.dat")

	answer := day12Part2(data)

	if answer != 28885 {
		t.Errorf("Test08Part2 returned %f, expected 28885", answer)
	}
}
