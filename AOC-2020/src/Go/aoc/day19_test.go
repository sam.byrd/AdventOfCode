package aoc

import (
	"testing"
)

func TestDay19Part2Example3(t *testing.T) {
	data := []string{
		"0: 4 1 5",
		"1: 2 3 | 3 2",
		"2: 4 4 | 5 5",
		"3: 4 5 | 5 4",
		"4: \"a\"",
		"5: \"b\"",
		"",
		"ababbb",
		"bababa",
		"abbbab",
		"aaabbb",
		"aaaabbb",
	}

	answer := Day19Part1(data)

	if answer != 2 {
		t.Errorf("Testday19Part1Example1 gave %d, expected 2", answer)
	}
}

func TestDay19Part1(t *testing.T) {

	data, _ := loadData("../../../data/day19.dat")

	answer := Day19Part1(data)

	if answer != 98621258158412 {
		t.Errorf("TestDay19Part1 returned %d, expected 98621258158412", answer)
	}
}

func TestDay19Part2(t *testing.T) {

	data, _ := loadData("../../../data/day19.dat")

	answer := Day19Part2(data)

	if answer != 241216538527890 {
		t.Errorf("TestDay19Part2 returned %d, expected 241216538527890", answer)
	}
}
