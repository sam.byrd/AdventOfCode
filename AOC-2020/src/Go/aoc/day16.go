package aoc

import (
	"fmt"
	"strconv"
	"strings"
)

/*
departure location: 29-766 or 786-950
departure station: 40-480 or 491-949
departure platform: 46-373 or 397-957
departure track: 33-657 or 673-970
departure date: 31-433 or 445-961
departure time: 33-231 or 250-966
arrival location: 48-533 or 556-974
arrival station: 42-597 or 620-957
arrival platform: 32-119 or 140-967
arrival track: 28-750 or 762-973
class: 26-88 or 101-950
duration: 30-271 or 293-974
price: 33-712 or 718-966
route: 49-153 or 159-953
row: 36-842 or 851-972
seat: 43-181 or 194-955
train: 29-500 or 513-964
type: 32-59 or 73-974
wagon: 47-809 or 816-957
zone: 44-451 or 464-955

your ticket:
151,103,173,199,211,107,167,59,113,179,53,197,83,163,101,149,109,79,181,73

nearby tickets:
339,870,872,222,255,276,706,890,583,718,924,118,201,141,59,581,931,143,221,919
*/

type MinMax struct {
	min int
	max int
}

type ValidationRule struct {
	code     string
	minmax1  MinMax
	minmax2  MinMax
	position int
}

type Element struct {
	name         string
	value        int
	rulesMatched []int
}

type Ticket struct {
	elements []Element
	valid    bool
	number   int
}

func processRule(input string) (rule ValidationRule) {
	parts := strings.Split(input, ":")
	rule.position = -1
	rule.code = parts[0]
	fmt.Sscanf(parts[1], "%d-%d or %d-%d", &rule.minmax1.min, &rule.minmax1.max, &rule.minmax2.min, &rule.minmax2.max)

	return
}
func validateElement(element Element, rules []ValidationRule) (valid bool) {
	valid = false
	for _, r := range rules {
		if (element.value >= r.minmax1.min && element.value <= r.minmax1.max) ||
			(element.value >= r.minmax2.min && element.value <= r.minmax2.max) {
			// fmt.Printf("  %d ==> matches %d-%d or %d-%d\n", e.value, r.minmax1.min, r.minmax1.max, r.minmax2.min, r.minmax2.max)
			valid = true
			break
		}
	}

	return
}

func validateField(rule ValidationRule, value int) bool {
	if (value >= rule.minmax1.min && value <= rule.minmax1.max) ||
		(value >= rule.minmax2.min && value <= rule.minmax2.max) {
		return true
	} else {
		return false
	}
}

func validateTicket(ticket Ticket, rules []ValidationRule) (int, Ticket, bool) {
	entropy := 0
	valid := true

	for ie, e := range ticket.elements {
		// fmt.Printf("Element %d\n  Rules:\n", ie)
		for ir := 0; ir < len(rules); ir++ {
			r := rules[ir]
			if validateField(r, e.value) {
				// fmt.Printf("    %d ==> matches %d-%d or %d-%d\n", e.value, r.minmax1.min, r.minmax1.max, r.minmax2.min, r.minmax2.max)
				ticket.elements[ie].rulesMatched = append(ticket.elements[ie].rulesMatched, ir)
				// break
			}
		}
		// fmt.Println("    matched: ", ticket.elements[ie].rulesMatched)

		if len(ticket.elements[ie].rulesMatched) == 0 {
			fmt.Printf("****  %d  **** no match\n", e.value)
			entropy += e.value
			valid = false
		} else if len(ticket.elements[ie].rulesMatched) != len(rules) {
			// fmt.Printf(" element %d, %d rules matched\n", ie, len(ticket.elements[ie].rulesMatched))
		}
	}

	return entropy, ticket, valid
}

func processTicket(input string) (ticket Ticket) {
	vals := strings.Split(input, ",")

	for _, val := range vals {
		element := Element{}
		element.value, _ = strconv.Atoi(val)
		ticket.elements = append(ticket.elements, element)
	}

	return
}

// Day16Part1 is ...
func Day16Part1(data []string) (answer int) {
	var rules = []ValidationRule{}
	// var myTicket Ticket
	// var otherTickets = []Ticket{}

	// grab the validation rules
	i := 0
	for data[i] != "" {
		rule := processRule(data[i])
		rules = append(rules, rule)
		i++
	}

	// skip blank and label
	i += 2

	// grab my ticket
	// myTicket := processTicket(data[i])
	i++

	// skip blank after myTicket and label
	i += 2

	// grab "other tickets"
	answer = 0
	for i < len(data) {
		ticket := processTicket(data[i])
		entropy, _, _ := validateTicket(ticket, rules)
		answer += entropy
		// otherTickets = append(otherTickets, ticket)
		i++
	}

	return

}

// Day16Part2 is ...
func Day16Part2(data []string) (answer int) {

	var rules = []ValidationRule{}
	var validTickets = []Ticket{}
	var myTicket = Ticket{}
	// var otherTickets = []Ticket{}

	// grab the validation rules
	i := 0
	for data[i] != "" {
		rule := processRule(data[i])
		// if len(rule.code) > 8 && rule.code[:9] == "departure" {
		rules = append(rules, rule)
		// }
		i++
	}

	// skip blank and label
	i += 2

	// grab my ticket
	// fmt.Printf("Ticket %d\n", len(validTickets))
	_, myTicket, _ = validateTicket(processTicket(data[i]), rules)
	myTicket.number = 0
	validTickets = append(validTickets, myTicket)
	// fmt.Printf("\n")
	i++

	// skip blank after myTicket and label
	i += 2

	// grab "other tickets"
	answer = 0
	for i < len(data) {
		_, ticket, valid := validateTicket(processTicket(data[i]), rules)
		ticket.number = len(validTickets)
		if valid == true {
			validTickets = append(validTickets, ticket)
		}
		i++
	}

	// find fields based on those that are valid for ALL tickets
	ruleToBadColumns := make(map[int]map[int]bool)
	for ir, r := range rules {
		ruleToBadColumns[ir] = make(map[int]bool)
		for _, t := range validTickets {
			for iv, v := range t.elements {
				if ruleToBadColumns[ir][iv] {
					continue
				}
				if !validateField(r, v.value) {
					ruleToBadColumns[ir][iv] = true
				}
			}
		}
	}

	// now see what all works
	ruleToCol := make(map[int]int)

	for len(ruleToCol) < len(rules) {
		for ir, r := range rules {
			// if this field is mapped, skip
			if _, ok := ruleToCol[ir]; ok {
				continue
			}

			bad := ruleToBadColumns[ir]
			if len(bad) == len(rules)-1 {
				colIndex := 0
				for i := 0; i < len(rules); i++ {
					if !bad[i] {
						colIndex = i
						ruleToCol[ir] = i
						r.position = i
						break
					}
				}

				// clean this column out of other fields
				for r2i, r2 := range rules {
					if r2.code == r.code {
						continue
					}
					ruleToBadColumns[r2i][colIndex] = true
				}

			}
		}
	}

	// loop over every position
	// for iPos := 0; iPos < len(myTicket.elements); iPos++ {
	// 	fmt.Printf("Pos: %d\n", iPos)
	// 	for ir, _ := range rules {
	// 		// if r.position == -1 {

	// 		validCount := 0
	// 		for _, t := range validTickets {
	// 			if containsInt(t.elements[iPos].rulesMatched, ir) {
	// 				validCount++
	// 			} else {
	// 				fmt.Printf("ticket %d -- not valid\n", t.number)
	// 			}
	// 		}
	// 		if validCount == len(validTickets) {
	// 			rules[ir].position = iPos
	// 			fmt.Printf(" Rule %d - pos %d\n", ir, iPos)
	// 			break
	// 		}
	// 		// }
	// 	}
	// }

	// now find all of the rules that start with departure
	answer = 1
	for _, r := range rules {

		// fmt.Printf("%s: %d\n", r.code, myTicket.elements[r.position].value)
		if len(r.code) > 8 && r.code[:9] == "departure" {
			if r.position == -1 {
				fmt.Printf(" invalid position - %s\n", r.code)
			} else {
				answer *= myTicket.elements[r.position].value
			}
		}
	}
	return
}
