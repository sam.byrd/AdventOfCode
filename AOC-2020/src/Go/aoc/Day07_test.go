package aoc

import "testing"

func TestDay07Example1(t *testing.T) {
	data := []string{
		"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags.",
	}

	answer := day07Part1(data, 9)

	if answer != 4 {
		t.Errorf("TestDay07Example1 gave %d, expected 4", answer)
	}
}

func TestDay07Example2(t *testing.T) {
	data := []string{
		"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags.",
	}

	answer := day07Part2(data, 9)

	if answer != 32 {
		t.Errorf("TestDay07Example2 gave %d, expected 32", answer)
	}
}

func TestDay07Part1(t *testing.T) {

	data, count := loadData("../../../data/day07.dat")

	answer := day07Part1(data, count)

	if answer != 172 {
		t.Errorf("Test07Part1 returned %d, expected 172", answer)
	}
}

func TestDay07Part2(t *testing.T) {

	data, count := loadData("../../../data/day07.dat")

	answer := day07Part2(data, count)

	if answer != 39645 {
		t.Errorf("Test07Part2 returned %d, expected 39645", answer)
	}
}
