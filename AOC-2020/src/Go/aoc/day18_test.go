package aoc

import (
	"testing"
)

func TestDay18Part1Example1(t *testing.T) {
	data := []string{
		"1 + 2 * 3 + 4 * 5 + 6",
	}

	answer := Day18Part1(data)

	if answer != 71 {
		t.Errorf("Testday18Part1Example1 gave %d, expected 71", answer)
	}
}

func TestDay18Part1Example2(t *testing.T) {
	data := []string{
		"1 + (2 * 3) + (4 * (5 + 6))",
	}

	answer := Day18Part1(data)

	if answer != 51 {
		t.Errorf("Testday18Part1Example2 gave %d, expected 51", answer)
	}
}

func TestDay18Part2Example1(t *testing.T) {
	data := []string{
		"1 + 2 * 3 + 4 * 5 + 6",
	}

	answer := Day18Part2(data)

	if answer != 231 {
		t.Errorf("Testday18Part2Example1 gave %d, expected 231", answer)
	}
}

func TestDay18Part2Example2(t *testing.T) {
	data := []string{
		"1 + (2 * 3) + (4 * (5 + 6))",
	}

	answer := Day18Part2(data)

	if answer != 51 {
		t.Errorf("Testday18Part2Example2 gave %d, expected 51", answer)
	}
}

func TestDay18Part2Example3(t *testing.T) {
	data := []string{
		"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
	}

	answer := Day18Part2(data)

	if answer != 23340 {
		t.Errorf("Testday18Part2Example3 gave %d, expected 23340", answer)
	}
}

func TestDay18Part1(t *testing.T) {

	data, _ := loadData("../../../data/day18.dat")

	answer := Day18Part1(data)

	if answer != 98621258158412 {
		t.Errorf("TestDay18Part1 returned %d, expected 98621258158412", answer)
	}
}

func TestDay18Part2(t *testing.T) {

	data, _ := loadData("../../../data/day18.dat")

	answer := Day18Part2(data)

	if answer != 241216538527890 {
		t.Errorf("TestDay18Part2 returned %d, expected 241216538527890", answer)
	}
}
