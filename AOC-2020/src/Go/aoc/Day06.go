package aoc

func day06Part1(input []string, count int) (totalCount int) {
	totalCount = 0
	counts := make(map[byte]int)

	for i := 0; i < count; i++ {
		if input[i] == "" {
			// get counts and clear
			totalCount += len(counts)

			counts = make(map[byte]int)
		} else {

			for j := 0; j < len(input[i]); j++ {
				counts[input[i][j]] = 1
			}
		}
	}

	totalCount += len(counts)

	return
}

func day06Part2(input []string, count int) (totalCount int) {
	totalCount = 0
	counts := make(map[byte]int)
	groupCount := 0

	for i := 0; i < count; i++ {
		if input[i] == "" {
			// get counts and clear

			for _, v := range counts {
				if v == groupCount {
					totalCount++
				}
			}

			counts = make(map[byte]int)
			groupCount = 0
		} else {
			groupCount++
			for j := 0; j < len(input[i]); j++ {
				letterCount, prs := counts[input[i][j]]
				if prs == false {
					counts[input[i][j]] = 1
				} else {
					counts[input[i][j]] = letterCount + 1
				}
			}
		}
	}

	for _, v := range counts {
		if v == groupCount {
			totalCount++
		}
	}

	return
}
