package aoc

import (
	"regexp"
	"strconv"
	"strings"
)

// day04part1 is ...
func day04part1(data []string, count int) (answer int) {

	// fmt.Printf("Processing %d lines\n", count)

	i := 0
	for i < count {
		passport := ""
		for i < count && data[i] != "" {
			passport += data[i]
			i++
		}

		//fmt.Printf("%s\n", passport)
		if strings.Contains(passport, "byr") &&
			strings.Contains(passport, "iyr") &&
			strings.Contains(passport, "eyr") &&
			strings.Contains(passport, "hgt") &&
			strings.Contains(passport, "hcl") &&
			strings.Contains(passport, "ecl") &&
			strings.Contains(passport, "pid") {
			answer++
		}

		i++
	}

	return answer
}

func dbg(data string) {
	// fmt.Println(data)
}

// day004part2 is ...
func day04part2(data []string, count int) (answer int) {
	//dbgFlag := true
	// fmt.Printf("Processing %d lines\n", count)

	i := 0
	for i < count {
		passport := ""
		for i < count && data[i] != "" {
			passport += " " + data[i]
			i++
		}

		byr := false
		iyr := false
		eyr := false
		hgt := false
		hcl := false
		ecl := false
		pid := false

		dbg(passport)
		parts := strings.Split(passport, " ")
		for j := 0; j < len(parts); j++ {
			parts2 := strings.Split(parts[j], ":")

			if parts2[0] == "hcl" && parts2[1][0] == '#' && len(parts2[1]) == 7 {
				for k := 1; k < 7; k++ {
					if parts2[1][k] == '0' || parts2[1][k] == '1' || parts2[1][k] == '2' || parts2[1][k] == '3' || parts2[1][k] == '4' || parts2[1][k] == '5' || parts2[1][k] == '6' ||
						parts2[1][k] == '7' || parts2[1][k] == '8' || parts2[1][k] == '9' || parts2[1][k] == 'a' || parts2[1][k] == 'b' || parts2[1][k] == 'c' || parts2[1][k] == 'd' ||
						parts2[1][k] == 'e' || parts2[1][k] == 'f' {
						dbg("  good hcl")
						// do nothing
					} else {
						dbg("  ===> bad hcl")
						break
					}
				}
				hcl = true
			} else if parts2[0] == "byr" {
				year, err := strconv.Atoi(parts2[1])
				if err == nil && year >= 1920 && year <= 2002 {
					dbg("  good byr")
					byr = true
				} else {
					dbg("  ===> bad byr")
				}
			} else if parts2[0] == "iyr" {
				year, err := strconv.Atoi(parts2[1])
				if err == nil && year >= 2010 && year <= 2020 {
					dbg("  good iyr")
					iyr = true
				} else {
					dbg("  ===> bad iyr")
				}
			} else if parts2[0] == "eyr" {
				year, err := strconv.Atoi(parts2[1])
				if err == nil && year >= 2020 && year <= 2030 {
					dbg("  good eyr")
					eyr = true
				} else {
					dbg("  ===> bad eyr")
				}
			} else if parts2[0] == "hgt" {
				height, err := strconv.Atoi(parts2[1][:len(parts2[1])-2])
				units := parts2[1][len(parts2[1])-2:]

				if err == nil && ((units == "cm" && height >= 150 && height <= 193) || (units == "in" && height >= 59 && height <= 76)) {
					dbg("  good hgt")
					hgt = true
				} else {
					dbg("  ===> bad hgt")
					// fmt.Printf("  unit - %s, height - %d", units, height)
				}
			} else if parts2[0] == "ecl" && (parts2[1] == "amb" || parts2[1] == "blu" || parts2[1] == "brn" || parts2[1] == "gry" || parts2[1] == "grn" || parts2[1] == "hzl" || parts2[1] == "oth") {
				dbg("good ecl")
				ecl = true
			} else if parts2[0] == "pid" {
				_, err := strconv.Atoi(parts2[1])
				if err == nil && len(parts2[1]) == 9 {
					dbg("good pid")
					pid = true
				} else {
					dbg("  ===> bad pid")
				}
			}
		}

		if strings.Contains(passport, "byr") &&
			strings.Contains(passport, "iyr") &&
			strings.Contains(passport, "eyr") &&
			strings.Contains(passport, "hgt") &&
			strings.Contains(passport, "hcl") &&
			strings.Contains(passport, "ecl") &&
			strings.Contains(passport, "pid") && hcl == true && byr == true && iyr == true && eyr == true && hgt == true && ecl == true && pid == true {
			dbg("  *** valid ***")
			answer++
		} else {
			dbg("  *** not valid ***")
		}
		dbg("")

		i++
	}

	return answer
}

// day04part2regex is ...
func day04part2regex(data []string, count int) (answer int) {
	//dbgFlag := true
	// fmt.Printf("Processing %d lines\n", count)

	i := 0
	var reBYR = regexp.MustCompile(`(?m).*byr:(19[2-9][0-9]|200[0-2]).*\b`)
	var reIYR = regexp.MustCompile(`(?m).*iyr:(20[1][0-9]|2020)\b`)
	var reEYR = regexp.MustCompile(`(?m).*eyr:(20[2][0-9]|2030)\b`)
	var reHGT = regexp.MustCompile(`(?m).*hgt:((1[5][0-9]|1[6-8][0-9]|19[0-3])cm|(59|[6][0-9]|[7][0-6])in)\b`)
	var reHCL = regexp.MustCompile(`(?m).*hcl:#[0-9a-f]{6}\b`)
	var reECL = regexp.MustCompile(`(?m).*ecl:(amb|blu|brn|gry|grn|hzl|oth)\b`)
	var rePID = regexp.MustCompile(`(?m).*pid:[0-9]{9}\b`)

	for i < count {
		passport := ""
		for i < count && data[i] != "" {
			passport += " " + data[i]
			i++
		}

		// fmt.Println(passport)

		//var str = `hcl:#341e13 ecl:lzr eyr:2024 iyr:2014 pid:161cm byr:1920 cid:59`

		// if reBYR.MatchString(passport) {
		// 	fmt.Println(" BYR good")
		// }
		// if reIYR.MatchString(passport) {
		// 	fmt.Println(" IYR good")
		// }
		// if reEYR.MatchString(passport) {
		// 	fmt.Println(" EYR good")
		// }
		// if reHGT.MatchString(passport) {
		// 	fmt.Println(" HGT good")
		// }
		// if reHCL.MatchString(passport) {
		// 	fmt.Println(" HCL good")
		// }
		// if reECL.MatchString(passport) {
		// 	fmt.Println(" ECL good")
		// }
		// if rePID.MatchString(passport) {
		// 	fmt.Println(" PID good")
		// }

		if reBYR.MatchString(passport) &&
			reIYR.MatchString(passport) &&
			reEYR.MatchString(passport) &&
			reHGT.MatchString(passport) &&
			reHCL.MatchString(passport) &&
			reECL.MatchString(passport) &&
			rePID.MatchString(passport) {
			// fmt.Println("  ===> VALID")
			answer++
		}

		// fmt.Println("")

		i++
	}

	return answer
}
