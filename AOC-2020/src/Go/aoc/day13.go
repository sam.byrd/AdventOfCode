package aoc

import (
	"strconv"
	"strings"
)

// day13Part1 is ...
func day13Part1(data []string) (answer int) {

	depart, _ := strconv.Atoi(data[0])

	busses := strings.Split(data[1], ",")

	minwait := 9999999
	minbus := 0

	for _, v := range busses {
		if v != "x" {
			busid, _ := strconv.Atoi(v)
			wait := busid - depart%busid
			if wait < minwait {
				minwait = wait
				minbus = busid
			}
		}
	}

	return minwait * minbus

}

// bDone := false
// for !bDone {
// 	time += bus1
// 	for i, v := range busses[1:] {
// 		if v != "x" {
// 			busX, _ := strconv.Atoi(v)
// 			if (time+1+i)%busX != 0 {
// 				break
// 			} else if (time+1+i)%busX == 0 && i+2 == len(busses) {
// 				bDone = true
// 				// break
// 			}
// 		}
// 	}
// day13Part2 is ...
func day13Part2(data []string) (answer int) {

	busses := strings.Split(data[1], ",")

	// bus1, _ := strconv.Atoi(busses[0])
	minTime := 1
	step := 1

	for timeslot, busNumS := range busses { // i = timeslot, bus = busNum
		if busNumS == "x" {
			continue
		}
		busNum, _ := strconv.Atoi(busNumS)
		for (minTime+timeslot)%busNum != 0 {
			minTime += step
		}
		step *= busNum
	}

	return minTime
}
