package aoc

import (
	"strconv"
	"strings"
)

func validate(msg string, rules map[int]map[int][]string, ruleNum int, pos int) bool {
	// valid := true
	// done := false

	// for ruleSet := range rules[ruleNum] {
	// }

	// if len(rules[ruleNum]) == 1 {
	// 	if len(rules[ruleNum][0]) == "a" {
	// 		if msg[pos] == "a" {
	// 			return true
	// 		} else {
	// 			return false
	// 		}
	// 	} else if len(rules[ruleNum][0]) == "b" {
	// 		if msg[pos] == "b" {
	// 			return true
	// 		} else {
	// 			return false
	// 		}
	// 	} else {

	// 	}
	// }
	// for valid && !done {

	// }
	return false
}

func buildMaps(data []string) (rules map[int]map[int][]string, messages []string) {
	rules = make(map[int]map[int][]string)

	i := 0
	for i = 0; i < len(data) && data[i] != ""; i++ {
		// 1: 2 "a" | 3 4
		parts := strings.Split(data[i], ":")
		// parts[0] = 1
		// parts[1] = 2 "a" | 3 4
		tempNum, _ := strconv.Atoi(parts[0])
		parts = strings.Split(parts[1], " | ")
		// parts[0] = 2 "a"
		// parts[1] = 3 4
		rules[tempNum] = make(map[int][]string)
		for vi, v := range parts {
			rules[tempNum][vi] = strings.Split(v, " ")
		}
	}

	return rules, data[i+1:]
}

// Day19Part1 is ...
func Day19Part1(data []string) (answer int) {
	// rules, messages := buildMaps(data)
	validMessages := make(map[string]bool)

	// for msg := range messages {
	// 	if validate(msg, rules) {
	// 		validMessages[msg] = true
	// 	}
	// }

	return len(validMessages)
}

// Day19Part2 is ...
func Day19Part2(data []string) (answer int) {

	return
}
