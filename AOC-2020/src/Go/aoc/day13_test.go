package aoc

import "testing"

func TestDay13Part1Example1(t *testing.T) {
	data := []string{
		"939",
		"7,13,x,x,59,x,31,19",
	}

	answer := day13Part1(data)

	if answer != 295 {
		t.Errorf("Testday13Example1 gave %d, expected 295", answer)
	}
}

func TestDay13Part2Example1(t *testing.T) {
	data := []string{
		"939",
		"7,13,x,x,59,x,31,19",
	}

	answer := day13Part2(data)

	if answer != 1068781 {
		t.Errorf("TestDay13Part2Example1 gave %d, expected 1068781", answer)
	}
}

func TestDay13Part1(t *testing.T) {

	data, _ := loadData("../../../data/day13.dat")

	answer := day13Part1(data)

	if answer != 1835 {
		t.Errorf("TestDay13Part1 returned %d, expected 1835", answer)
	}
}

func TestDay13Part2(t *testing.T) {

	data, _ := loadData("../../../data/day13.dat")

	answer := day13Part2(data)

	if answer != 247086664214628 {
		t.Errorf("TestDay13Part2 returned %d, expected 247086664214628", answer)
	}
}
