package aoc

import (
	"strings"
	"testing"
)

func TestDay20Part1Example1(t *testing.T) {
	data := `Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...`

	answer := Day20Part1(strings.Split(data, "\n"))

	if answer != 2 {
		t.Errorf("TestDay20Part1Example1 gave %d, expected 2", answer)
	}
}

func TestDay20Part1Example2(t *testing.T) {
	data := `Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...`

	answer := Day20Part1(strings.Split(data, "\n"))

	if answer != 20899048083289 {
		t.Errorf("TestDay20Part1Example2 gave %d, expected 20899048083289", answer)
	}
}

func TestDay20Part1ExampleFlipX(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	answer := transform(images[0], FlipX)
	printTile(answer)

}

func TestDay20Part1ExampleFlipY(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	answer := transform(images[0], FlipY)
	printTile(answer)

}

func TestDay20Part1ExampleRotate180(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	answer := transform(images[0], CW180)
	printTile(answer)

}

func TestDay20Part1ExampleRotateCW90(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	answer := transform(images[0], CW90)
	printTile(answer)

}

func TestDay20Part1ExampleRotateCCW90(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	answer := transform(images[0], CCW90)
	printTile(answer)

}

func TestDay20Part1ExampleCompareWE(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	tile2 := transform(images[0], FlipX)

	printTile(images[0])
	printTile(tile2)

	if !compareEdge(images[0], tile2, WE) {
		t.Errorf("WE compare expected true, returned false")
	}
}

func TestDay20Part1ExampleCompareEW(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	tile2 := transform(images[0], FlipX)

	printTile(images[0])
	printTile(tile2)

	if !compareEdge(tile2, images[0], EW) {
		t.Errorf("EW compare expected true, returned false")
	}
}

func TestDay20Part1ExampleCompareSN(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	tile2 := transform(images[0], FlipY)

	printTile(images[0])
	printTile(tile2)

	if !compareEdge(tile2, images[0], SN) {
		t.Errorf("SN compare expected true, returned false")
	}
}

func TestDay20Part1ExampleCompareNS(t *testing.T) {
	data := `Tile 1:
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY

`
	images := loadImages(strings.Split(data, "\n"))
	tile2 := transform(images[0], FlipY)

	printTile(images[0])
	printTile(tile2)

	if !compareEdge(tile2, images[0], NS) {
		t.Errorf("NS compare expected true, returned false")
	}
}

func TestDay20Part1(t *testing.T) {

	data, _ := loadData("../../../data/day20.dat")

	answer := Day20Part1(data)

	if answer != 98621258158412 {
		t.Errorf("TestDay20Part1 returned %d, expected 98621258158412", answer)
	}
}

func TestDay20Part2(t *testing.T) {

	data, _ := loadData("../../../data/day20.dat")

	answer := Day20Part2(data)

	if answer != 241216538527890 {
		t.Errorf("TestDay20Part2 returned %d, expected 241216538527890", answer)
	}
}
