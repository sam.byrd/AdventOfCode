package aoc

import (
	"strconv"
)

func day01solve1(data []string, count int, target int) (answer int) {
	for i := 0; i < count; i++ {
		num1, _ := strconv.Atoi(data[i])
		for j := 1; j < count; j++ {
			if i != j {
				num2, _ := strconv.Atoi(data[j])
				if num1+num2 == target {
					answer = num1 * num2
				}
			}
		}
	}

	return
}

func day01solve2(data []string, count int, target int) (answer int) {
	for i := 0; i < count; i++ {
		num1, _ := strconv.Atoi(data[i])
		for j := 1; j < count; j++ {
			if i != j {
				num2, _ := strconv.Atoi(data[j])
				for k := 1; k < count; k++ {
					if i != k && j != k {
						num3, _ := strconv.Atoi(data[k])
						if num1+num2+num3 == target {
							answer = num1 * num2 * num3
							break
						}
					}
				}
			}
		}
	}

	return
}
