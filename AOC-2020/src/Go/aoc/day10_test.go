package aoc

import "testing"

func TestDay10Part1Example1(t *testing.T) {
	data := []string{
		"16",
		"10",
		"15",
		"5",
		"1",
		"11",
		"7",
		"19",
		"6",
		"12",
		"4",
	}

	answer := day10Part1(data)

	if answer != 35 {
		t.Errorf("Testday10Example1 gave %d, expected 35", answer)
	}
}

func TestDay10Part1Example2(t *testing.T) {
	data := []string{
		"28",
		"33",
		"18",
		"42",
		"31",
		"14",
		"46",
		"20",
		"48",
		"47",
		"24",
		"23",
		"49",
		"45",
		"19",
		"38",
		"39",
		"11",
		"1",
		"32",
		"25",
		"35",
		"8",
		"17",
		"7",
		"9",
		"4",
		"2",
		"34",
		"10",
		"3",
	}

	answer := day10Part1(data)

	if answer != 220 {
		t.Errorf("Testday10Example2 gave %d, expected 220", answer)
	}
}

func TestDay10Part2Example1(t *testing.T) {
	data := []string{
		"28",
		"33",
		"18",
		"42",
		"31",
		"14",
		"46",
		"20",
		"48",
		"47",
		"24",
		"23",
		"49",
		"45",
		"19",
		"38",
		"39",
		"11",
		"1",
		"32",
		"25",
		"35",
		"8",
		"17",
		"7",
		"9",
		"4",
		"2",
		"34",
		"10",
		"3",
	}

	answer := day10Part2(data)

	if answer != float64(19208) {
		t.Errorf("Testday10Part2Example1 gave %f, expected 19208", answer)
	}
}

func TestDay10Part1(t *testing.T) {

	data, _ := loadData("../../../data/day10.dat")

	answer := day10Part1(data)

	if answer != 2112 {
		t.Errorf("Test08Part1 returned %d, expected 2112", answer)
	}
}

func TestDay10Part2(t *testing.T) {

	data, _ := loadData("../../../data/day10.dat")

	answer := day10Part2(data)

	if answer != float64(3022415986688) {
		t.Errorf("Test08Part2 returned %f, expected 3022415986688", answer)
	}
}
