package aoc

func getRange(data byte, beg int, end int) (newbeg int, newend int) {
	if data == 'F' || data == 'L' {
		newbeg = beg
		newend = beg + (end-beg)/2
	} else {
		newbeg = beg + (end-beg)/2 + 1
		newend = end
	}

	return
}

func getCol(data string) int {
	beg := 0
	end := 7

	for i := 0; i < 3; i++ {
		beg, end = getRange(data[i], beg, end)
	}

	return beg
}

func getRow(data string) int {
	beg := 0
	end := 127

	for i := 0; i < 7; i++ {
		beg, end = getRange(data[i], beg, end)
		//fmt.Printf("%d - %d\n", beg, end)
	}

	return beg
}

func day05part1(data []string, count int) int {

	// fmt.Printf("Processing %d lines\n", count)
	maxSeat := -1
	for i := 0; i < count; i++ {
		row := getRow(data[i][:7])
		col := getCol(data[i][7:])
		seat := row*8 + col
		if seat > maxSeat {
			maxSeat = seat
		}
	}
	return maxSeat
}

func day05part2(data []string, count int) (answer int) {

	// fmt.Printf("Processing %d lines\n", count)

	var seats [1024]int

	i := 0

	for i = 0; i < 1024; i++ {
		seats[i] = -1
	}

	for i = 0; i < count; i++ {
		row := getRow(data[i][:7])
		col := getCol(data[i][7:])
		seat := row*8 + col
		seats[seat] = 1
	}

	for i = 1; i < count-1; i++ {
		if seats[i-1] != -1 && seats[i] == -1 && seats[i+1] != -1 {
			break
		}
	}

	return i
}
