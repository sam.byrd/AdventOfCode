package aoc

import (
	"strconv"
)

// day15Part1 is ...
func day15Part1(data []string, target int) (answer int) {

	var nummap = make(map[int]int)

	turnNum := 1
	lastNum := 0
	for _, v := range data {
		val, _ := strconv.Atoi(v)
		nummap[val] = turnNum
		turnNum++
		lastNum = val
	}

	// turn after starting is 0
	lastNum = 0
	diff := 0
	for turnNum < target {
		if check, ok := nummap[lastNum]; ok {
			diff = turnNum - check
		} else {
			diff = 0
		}

		nummap[lastNum] = turnNum
		lastNum = diff
		turnNum++
	}

	return diff

}

// day15Part2 is ...
func day15Part2(data []string) (answer int) {

	return
}
