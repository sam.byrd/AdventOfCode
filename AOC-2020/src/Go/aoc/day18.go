package aoc

import (
	"strconv"
	"strings"
)

func calculate(tokens []string) (result []string) {
	result = []string{}
	// check for ( )
	i := 0
	j := 0
	for i < len(tokens) {
		if tokens[i] == ")" {
			// work back to the opening
			j = i - 1
			for j >= 0 {
				if tokens[j] == "(" {
					newTokens := append(calculate(tokens[j+1:i]), tokens[i+1:]...)
					tokens = append(tokens[:j], newTokens...)
					i = 0
					break
				}
				j--
			}
		}
		i++
	}

	i = 1
	left, _ := strconv.Atoi(tokens[0])
	for i < len(tokens)-1 {
		right, _ := strconv.Atoi(tokens[i+1])
		switch tokens[i] {
		case "+":
			left += right
		case "*":
			left *= right
		}

		i += 2
	}

	result = append(result, strconv.Itoa(left))

	return result
}

func calculate2(tokens []string) (result []string) {
	result = []string{}
	// check for ( )
	i := 0
	j := 0
	for i < len(tokens) {
		if tokens[i] == ")" {
			// work back to the opening
			j = i - 1
			for j >= 0 {
				if tokens[j] == "(" {
					newTokens := append(calculate2(tokens[j+1:i]), tokens[i+1:]...)
					tokens = append(tokens[:j], newTokens...)
					i = 0
					break
				}
				j--
			}
		}
		i++
	}

	// do all additions
	i = 1
	left := 0
	for i < len(tokens) {
		if tokens[i] == "+" {
			left, _ = strconv.Atoi(tokens[i-1])
			right, _ := strconv.Atoi(tokens[i+1])
			left += right

			tempTokens := []string{}
			tempTokens = append(tempTokens, strconv.Itoa(left))
			tempTokens = append(tokens[:i-1], tempTokens...)
			tokens = append(tempTokens, tokens[i+2:]...)
		} else {
			i += 2
		}
	}

	// do all multiplication
	i = 1
	for i < len(tokens) {
		if tokens[i] == "*" {
			left, _ = strconv.Atoi(tokens[i-1])
			right, _ := strconv.Atoi(tokens[i+1])
			left *= right

			tempTokens := []string{}
			tempTokens = append(tempTokens, strconv.Itoa(left))
			tempTokens = append(tokens[:i-1], tempTokens...)
			tokens = append(tempTokens, tokens[i+2:]...)
		} else {
			i += 2
		}
	}

	result = append(result, strconv.Itoa(left))

	return result
}

func processLine(input string) (result []string) {
	var tokens = strings.Split(input, " ")

	for i := 0; i < len(tokens); i++ {
		v := tokens[i]
		if len(v) > 1 {
			tokenTemp := []string{}
			for si := 0; si < len(v); si++ {
				tokenTemp = append(tokenTemp, v[si:si+1])
			}
			tokenTemp = append(tokenTemp, tokens[i+1:]...)
			tokens = append(tokens[:i], tokenTemp...)
			tokenTemp = nil
		}
	}

	return tokens

}

// Day18Part1 is ...
func Day18Part1(data []string) (answer int) {

	for _, v := range data {

		temp, _ := strconv.Atoi(calculate(processLine(v))[0])
		answer += temp
	}
	return

}

// Day18Part2 is ...
func Day18Part2(data []string) (answer int) {

	for _, v := range data {
		temp, _ := strconv.Atoi(calculate2(processLine(v))[0])
		answer += temp
	}
	return
}
