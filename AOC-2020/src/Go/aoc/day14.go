package aoc

import (
	"fmt"
	"math"
	"strconv"
)

// day14Part1 is ...
func day14Part1(data []string) (answer int64) {
	var memory = make(map[int64]int64)

	currMask := ""
	numbS := ""

	mempos := int64(0)
	val := int64(0)
	valTemp := 0

	for _, v := range data {
		answer = 0
		if v[:4] == "mask" {
			currMask = v[7:]
		} else {
			fmt.Sscanf(v, "mem[%d] = %v", &mempos, &val)
			numbS = fmt.Sprintf("%064b", val)
			val = 0
			for i := 35; i >= 0; i-- {
				if currMask[i:i+1] == "X" {
					valTemp, _ = strconv.Atoi(numbS[i+28 : i+29])
				} else {
					valTemp, _ = strconv.Atoi(currMask[i : i+1])
				}

				if valTemp == 1 {
					answer = answer + int64(math.Pow(2, float64(35-i)))
				}
			}
			memory[mempos] = answer
		}
	}

	answer = 0
	for _, memval := range memory {
		answer += memval
	}

	return

}

// day14Part2 is ...
func day14Part2(data []string) (answer int64) {
	var memory = make(map[int64]int64)

	currMask := ""
	numbS := ""

	mempos := int64(0)
	val := int64(0)
	valTemp := 0

	for _, v := range data {
		memList := []string{}
		if v[:4] == "mask" {
			currMask = v[7:]
		} else {
			fmt.Sscanf(v, "mem[%d] = %v", &mempos, &val)
			numbS = fmt.Sprintf("%064b", mempos)

			memList = append(memList, numbS)

			for iBit := 35; iBit >= 0; iBit-- {
				memListLen := len(memList)
				tempMemList := []string{}
				for iMemList := 0; iMemList < memListLen; iMemList++ {
					if currMask[iBit:iBit+1] == "X" {
						// need to write out both options
						if iBit == 35 {
							// temp :=
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"0")
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"1")
						} else {
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"0"+memList[iMemList][iBit+29:])
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"1"+memList[iMemList][iBit+29:])
						}
					} else if currMask[iBit:iBit+1] == "1" {
						if iBit == 35 {
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"1")
						} else {
							tempMemList = append(tempMemList, memList[iMemList][:iBit+28]+"1"+memList[iMemList][iBit+29:])
						}
					} else {
						// leave it alone
					}
				}
				if len(tempMemList) > 0 {
					memList = nil
					for _, v := range tempMemList {
						memList = append(memList, v)
					}
				}
				tempMemList = nil
			}

			// now convert these to numbers and write
			for iMemList := 0; iMemList < len(memList); iMemList++ {
				answer = 0
				for i := 35; i >= 0; i-- {
					valTemp, _ = strconv.Atoi(memList[iMemList][i+28 : i+29])
					if valTemp == 1 {
						answer = answer + int64(math.Pow(2, float64(35-i)))
					}
				}
				memory[answer] = val
			}
		}
	}

	answer = 0
	for _, memval := range memory {
		answer += memval
	}

	return
}
