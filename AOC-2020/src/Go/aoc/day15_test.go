package aoc

import (
	"strings"
	"testing"
)

func TestDay15Part1Example1(t *testing.T) {
	data := []string{
		"0,3,6",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 436 {
		t.Errorf("Testday15Part1Example1 gave %d, expected 436", answer)
	}
}

func TestDay15Part1Example2(t *testing.T) {
	data := []string{
		"1,3,2",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 1 {
		t.Errorf("Testday15Part1Example2 gave %d, expected 1", answer)
	}
}

func TestDay15Part1Example3(t *testing.T) {
	data := []string{
		"2,1,3",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 10 {
		t.Errorf("Testday15Part1Example3 gave %d, expected 10", answer)
	}
}

func TestDay15Part1Example4(t *testing.T) {
	data := []string{
		"1,2,3",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 27 {
		t.Errorf("Testday15Part1Example4 gave %d, expected 27", answer)
	}
}

func TestDay15Part1Example5(t *testing.T) {
	data := []string{
		"2,3,1",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 78 {
		t.Errorf("Testday15Part1Example5 gave %d, expected 78", answer)
	}
}

func TestDay15Part1Example6(t *testing.T) {
	data := []string{
		"3,2,1",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 438 {
		t.Errorf("Testday15Part1Example6 gave %d, expected 438", answer)
	}
}

func TestDay15Part1Example7(t *testing.T) {
	data := []string{
		"3,1,2",
	}

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 1836 {
		t.Errorf("Testday15Part1Example7 gave %d, expected 1836", answer)
	}
}

func TestDay15Part2Example1(t *testing.T) {
	data := []string{
		"0,3,6",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 175594 {
		t.Errorf("TestDay15Part2Example1 gave %d, expected 175594", answer)
	}
}

func TestDay15Part2Example2(t *testing.T) {
	data := []string{
		"1,3,2",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 2578 {
		t.Errorf("Testday15Part2Example2 gave %d, expected 2578", answer)
	}
}

func TestDay15Part2Example3(t *testing.T) {
	data := []string{
		"2,1,3",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 3544142 {
		t.Errorf("Testday15Part2Example3 gave %d, expected 3544142", answer)
	}
}

func TestDay15Part2Example4(t *testing.T) {
	data := []string{
		"1,2,3",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 261214 {
		t.Errorf("Testday15Part2Example4 gave %d, expected 261214", answer)
	}
}

func TestDay15Part2Example5(t *testing.T) {
	data := []string{
		"2,3,1",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 6895259 {
		t.Errorf("Testday15Part2Example5 gave %d, expected 6895259", answer)
	}
}

func TestDay15Part2Example6(t *testing.T) {
	data := []string{
		"3,2,1",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 18 {
		t.Errorf("Testday15Part2Example6 gave %d, expected 18", answer)
	}
}

func TestDay15Part2Example7(t *testing.T) {
	data := []string{
		"3,1,2",
	}

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 362 {
		t.Errorf("Testday15Part2Example6 gave %d, expected 362", answer)
	}
}

func TestDay15Part1(t *testing.T) {

	data, _ := loadData("../../../data/day15.dat")

	answer := day15Part1(strings.Split(data[0], ","), 2020)

	if answer != 253 {
		t.Errorf("TestDay15Part1 returned %d, expected 253", answer)
	}
}

func TestDay15Part2(t *testing.T) {

	data, _ := loadData("../../../data/day15.dat")

	answer := day15Part1(strings.Split(data[0], ","), 30000000)

	if answer != 13710 {
		t.Errorf("TestDay15Part2 returned %d, expected 13710", answer)
	}
}
