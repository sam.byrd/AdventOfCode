package aoc

import "testing"

func TestDay06Example1(t *testing.T) {
	data := []string{"abc",
		"",
		"a",
		"b",
		"c",
		"",
		"ab",
		"ac",
		"",
		"a",
		"a",
		"a",
		"a",
		"",
		"b"}

	answer := day06Part1(data, 15)

	if answer != 11 {
		t.Errorf("TestDay06Example1 gave %d, expected 11", answer)
	}
}

func TestDay06Example2(t *testing.T) {
	data := []string{"abc",
		"",
		"a",
		"b",
		"c",
		"",
		"ab",
		"ac",
		"",
		"a",
		"a",
		"a",
		"a",
		"",
		"b"}

	answer := day06Part2(data, 15)

	if answer != 6 {
		t.Errorf("TestDay06Example2 gave %d, expected 6", answer)
	}
}

func TestDay06Part1(t *testing.T) {

	data, count := loadData("../../../data/day06.dat")

	answer := day06Part1(data, count)

	if answer != 6259 {
		t.Errorf("Test06Part1 returned %d, expected 6259", answer)
	}
}

func TestDay06Part2(t *testing.T) {

	data, count := loadData("../../../data/day06.dat")

	answer := day06Part2(data, count)

	if answer != 3178 {
		t.Errorf("Test06Part1 returned %d, expected 3178", answer)
	}
}
