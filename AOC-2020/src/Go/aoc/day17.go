package aoc

import (
	"fmt"
)

type point3 struct {
	x int
	y int
	z int
}

type point4 struct {
	x int
	y int
	z int
	w int
}

type battery3 struct {
	grid map[point3]bool
	min  point3
	max  point3
}

type battery4 struct {
	grid map[point4]bool
	min  point4
	max  point4
}

func initialize(data []string) (battery map[point3]bool) {
	battery = map[point3]bool{}
	for y := 0; y < len(data); y++ {
		for x := 0; x < len(data[y]); x++ {
			if data[y][x] == '#' {
				// p := point3{x: x, y: y, z: 0}
				battery[point3{x: x, y: y, z: 0}] = true
			}
		}
	}
	return
}

func initialize4(data []string) (battery map[point4]bool) {
	battery = map[point4]bool{}
	for y := 0; y < len(data); y++ {
		for x := 0; x < len(data[y]); x++ {
			if data[y][x] == '#' {
				battery[point4{x: x, y: y, z: 0, w: 0}] = true
			}
		}
	}
	return
}

func analyze(battery map[point3]bool, min point3, max point3) (newBatt map[point3]bool, newmin point3, newmax point3) {
	newBatt = map[point3]bool{}
	fmt.Printf("Analyzing from (%d,%d,%d) to (%d,%d,%d)\n", min.x-1, min.y-1, min.z-1, max.x+1, max.y+2, max.z+1)
	for x := min.x - 1; x <= max.x+1; x++ {
		for y := min.y - 1; y <= max.y+1; y++ {
			for z := min.z - 1; z <= max.z+1; z++ {
				p := point3{x: x, y: y, z: z}
				s := battery[p]
				// if s {
				// 	fmt.Printf("(%d,%d,%d) - #\n", x, y, z)
				// } else {
				// 	fmt.Printf("(%d,%d,%d) - .\n", x, y, z)
				// }
				an := activeNeighbors(battery, p)
				if s == true && (an == 2 || an == 3) {
					// fmt.Printf("  => #\n")
					newBatt[p] = true

				} else if s == false && an == 3 {
					// fmt.Printf("  => #\n")
					newBatt[p] = true
				}
			}
			fmt.Println()
		}
		fmt.Println()
	}

	newmin = point3{x: 99999, y: 99999, z: 99999}
	newmax = point3{x: -99999, y: -99999, z: -99999}
	for pv := range newBatt {
		if pv.x < newmin.x {
			newmin.x = pv.x
		}
		if pv.y < newmin.y {
			newmin.y = pv.y
		}
		if pv.z < newmin.z {
			newmin.z = pv.z
		}
		if pv.x > newmax.x {
			newmax.x = pv.x
		}
		if pv.y > newmax.y {
			newmax.y = pv.y
		}
		if pv.z > newmax.z {
			newmax.z = pv.z
		}
	}

	return
}

func analyze4(battery map[point4]bool, min point4, max point4) (newBatt map[point4]bool, newmin point4, newmax point4) {
	newBatt = map[point4]bool{}
	fmt.Printf("Analyzing from (%d,%d,%d,%d) to (%d,%d,%d,%d)\n", min.x-1, min.y-1, min.z-1, min.w-1, max.x+1, max.y+2, max.z+1, max.w+1)
	for x := min.x - 1; x <= max.x+1; x++ {
		for y := min.y - 1; y <= max.y+1; y++ {
			for z := min.z - 1; z <= max.z+1; z++ {
				for w := min.w - 1; w <= max.w+1; w++ {
					p := point4{x: x, y: y, z: z, w: w}
					s := battery[p]
					// if s {
					// 	fmt.Printf("(%d,%d,%d) - #\n", x, y, z)
					// } else {
					// 	fmt.Printf("(%d,%d,%d) - .\n", x, y, z)
					// }
					an := activeNeighbors4(battery, p)
					if s == true && (an == 2 || an == 3) {
						// fmt.Printf("  => #\n")
						newBatt[p] = true

					} else if s == false && an == 3 {
						// fmt.Printf("  => #\n")
						newBatt[p] = true
					}
				}
			}
			// fmt.Println()
		}
		// fmt.Println()
	}

	newmin = point4{x: 99999, y: 99999, z: 99999, w: 99999}
	newmax = point4{x: -99999, y: -99999, z: -99999, w: -99999}
	for pv := range newBatt {
		if pv.x < newmin.x {
			newmin.x = pv.x
		}
		if pv.y < newmin.y {
			newmin.y = pv.y
		}
		if pv.z < newmin.z {
			newmin.z = pv.z
		}
		if pv.w < newmin.w {
			newmin.w = pv.w
		}
		if pv.x > newmax.x {
			newmax.x = pv.x
		}
		if pv.y > newmax.y {
			newmax.y = pv.y
		}
		if pv.z > newmax.z {
			newmax.z = pv.z
		}
		if pv.w > newmax.w {
			newmax.w = pv.w
		}
	}

	return
}

func activeNeighbors(battery map[point3]bool, p point3) (count int) {
	// go through the 26 "neighbors"
	for x := p.x - 1; x <= p.x+1; x++ {
		for y := p.y - 1; y <= p.y+1; y++ {
			for z := p.z - 1; z <= p.z+1; z++ {
				tp := point3{x: x, y: y, z: z}
				// fmt.Printf("  (%d,%d,%d) - %t\n", x, y, z, battery[tp])
				if !(x == p.x && y == p.y && z == p.z) && battery[tp] {
					count++
				}
			}
		}
	}
	fmt.Printf("  c: %d\n", count)
	return
}

func activeNeighbors4(battery map[point4]bool, p point4) (count int) {
	// go through the 26 "neighbors"
	for x := p.x - 1; x <= p.x+1; x++ {
		for y := p.y - 1; y <= p.y+1; y++ {
			for z := p.z - 1; z <= p.z+1; z++ {
				for w := p.w - 1; w <= p.w+1; w++ {
					tp := point4{x: x, y: y, z: z, w: w}
					// fmt.Printf("  (%d,%d,%d) - %t\n", x, y, z, battery[tp])
					if !(x == p.x && y == p.y && z == p.z && w == p.w) && battery[tp] {
						count++
					}
				}
			}
		}
	}
	// fmt.Printf("  c: %d\n", count)
	return
}

func print(battery map[point3]bool, min point3, max point3) {
	for z := min.z; z <= max.z; z++ {
		fmt.Printf("z: %d\n", z)
		for y := min.y; y <= max.y; y++ {
			for x := min.x; x <= max.x; x++ {
				// p := point3{x:x,y:y,z:z}
				if battery[point3{x: x, y: y, z: z}] {
					fmt.Printf("#")
				} else {
					fmt.Printf(".")
				}
			}
			fmt.Printf("\n")
		}
		fmt.Printf("\n\n")
	}

}

func print4(battery map[point4]bool, min point4, max point4) {
	for w := min.w; w <= max.w; w++ {
		fmt.Printf("w: %d\n", w)
		for z := min.z; z <= max.z; z++ {
			fmt.Printf("z: %d\n", z)
			for y := min.y; y <= max.y; y++ {
				for x := min.x; x <= max.x; x++ {
					// p := point3{x:x,y:y,z:z}
					if battery[point4{x: x, y: y, z: z, w: w}] {
						fmt.Printf("#")
					} else {
						fmt.Printf(".")
					}
				}
				fmt.Printf("\n")
			}
			fmt.Printf("\n")
		}
		fmt.Printf("\n\n")
	}

}

// Day17Part1 is ...
func Day17Part1(data []string) (answer int) {
	var battery = make(map[point3]bool)
	var min = point3{0, 0, 0}
	var max = point3{len(data[0]) - 1, len(data) - 1, 0}

	battery = initialize(data)
	// fmt.Println("State after init")
	// print(battery, min, max)

	for c := 0; c < 6; c++ {
		battery, min, max = analyze(battery, min, max)

		// fmt.Printf("State after %d cycle", c)
		// print(battery, min, max)
		// fmt.Println()
	}

	return len(battery)
}

// Day17Part2 is ...
func Day17Part2(data []string, cycles int) (answer int) {
	var battery = make(map[point4]bool)
	var min = point4{0, 0, 0, 0}
	var max = point4{len(data[0]) - 1, len(data) - 1, 0, 0}

	battery = initialize4(data)
	fmt.Println("State after init")
	print4(battery, min, max)

	for c := 0; c < cycles; c++ {
		battery, min, max = analyze4(battery, min, max)

		fmt.Printf("State after %d cycles\n", c)
		print4(battery, min, max)
		fmt.Println()
	}

	return len(battery)
}
