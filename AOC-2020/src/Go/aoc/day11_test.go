package aoc

import "testing"

func TestDay11Part1Example1(t *testing.T) {
	data := []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}

	answer := day11Part1(data)

	if answer != 37 {
		t.Errorf("Testday11Example1 gave %d, expected 37", answer)
	}
}

func TestDay11Part2Example1(t *testing.T) {
	data := []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}

	answer := day11Part2(data)

	if answer != 26 {
		t.Errorf("Testday11Part2Example1 gave %d, expected 26", answer)
	}
}

func TestDay11Part2Example2(t *testing.T) {
	data := []string{
		".......#.",
		"...#.....",
		".#.......",
		".........",
		"..#L....#",
		"....#....",
		".........",
		"#........",
		"...#.....",
	}

	var yard [102][102]byte
	for i := 0; i < len(data); i++ {
		parseLine(&yard, i+1, data[i])
	}

	answer := countVisibleAround(yard, len(data), len(data[0]), 5, 4)

	if answer != 8 {
		t.Errorf("TestDay11Part2Example2 gave %d, expected 8", answer)
	}
}

func TestDay11Part2Example3(t *testing.T) {
	data := []string{
		"#####",
		"#####",
		"##.##",
		"#####",
		"#####",
		"#####",
	}

	var yard [102][102]byte
	for i := 0; i < len(data); i++ {
		parseLine(&yard, i+1, data[i])
	}

	answer := countVisibleAround(yard, len(data), len(data[0]), 5, 1)

	if answer != 5 {
		t.Errorf("TestDay11Part2Example3 gave %d, expected 5", answer)
	}
}

func TestDay11Part1(t *testing.T) {

	data, _ := loadData("../../../data/day11.dat")

	answer := day11Part1(data)

	if answer != 2368 {
		t.Errorf("Test08Part1 returned %d, expected 2368", answer)
	}
}

func TestDay11Part2(t *testing.T) {

	data, _ := loadData("../../../data/day11.dat")

	answer := day11Part2(data)

	if answer != 2124 {
		t.Errorf("Test08Part2 returned %d, expected 2124", answer)
	}
}
