package aoc

func day03solve1(data []string, count int, rowChange int, colChange int) (treeCount int) {
	maxCol := len(data[0])

	currCol := 0

	treeCount = 0

	for currRow := 0; currRow < count; currRow += rowChange {
		//fmt.Printf("row: %d, col:%d, %s\n", currRow, currCol, data[currRow])
		if data[currRow][currCol] == '#' {
			treeCount++
		}
		//fmt.Printf("  spot: %c, treeCount: %d\n", data[currRow][currCol], treeCount)

		currCol += colChange

		if currCol >= maxCol {
			currCol = currCol - maxCol
		}
	}

	return
}

func day03solve2(data []string, count int) (answer int) {
	answer11 := day03solve1(data, count, 1, 1)
	//fmt.Printf("1x1: %d\n", answer11)
	answer13 := day03solve1(data, count, 1, 3)
	//fmt.Printf("1x3: %d\n", answer13)
	answer15 := day03solve1(data, count, 1, 5)
	//fmt.Printf("1x5: %d\n", answer15)
	answer17 := day03solve1(data, count, 1, 7)
	//fmt.Printf("1x7: %d\n", answer17)
	answer21 := day03solve1(data, count, 2, 1)
	//fmt.Printf("2x1: %d\n", answer21)

	return answer11 * answer13 * answer15 * answer17 * answer21
}
