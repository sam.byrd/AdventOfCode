package aoc

import (
	"fmt"
	"strconv"
	"strings"
)

// Bag is ...
type Bag struct {
	qty  int
	desc string
}

// day07Part1 is ...
func day07Part1(data []string, count int) (answer int) {
	//qty := 0
	color1 := ""
	color2 := ""

	//outer := ""

	bagList := []string{"shiny gold"}
	//	bagSize := 1

	for j := 0; j < len(bagList); j++ {
		// find all the bags taht can contain this bag
		for i := 0; i < count; i++ {
			parts := strings.Split(data[i], " contain ")
			//outer = parts[0]

			// does parts[1] contain "shiny gold"
			if strings.Contains(parts[1], bagList[j]) {
				_, err := fmt.Sscanf(parts[0], "%s %s bags", &color1, &color2)
				if err == nil {
					if !contains(bagList, color1+" "+color2) {
						bagList = append(bagList, color1+" "+color2)
						// fmt.Println(color1, " ", color2)
					}
				}
			}
		}
	}

	// find how all to hold a "shiny gold bag"

	// fmt.Println(bagList)

	return len(bagList) - 1
}

var bagMap = make(map[string][]Bag)

func day07Part2(data []string, count int) (answer int) {
	// build a map of all of the bags
	for i := 0; i < count; i++ {
		parts := strings.Split(data[i], " bags contain ")
		//outer = parts[0]
		//inner = parts[1]

		bagList := []Bag{}

		if strings.Contains(parts[1], "no other bags") {
			bagMap[parts[0]] = bagList
		} else {
			bags := strings.Split(parts[1], ", ")
			for _, s := range bags {

				bagParts := strings.Split(s, " ")
				bagQty, _ := strconv.Atoi(bagParts[0])
				bagList = append(bagList, Bag{qty: bagQty, desc: bagParts[1] + " " + bagParts[2]})
			}
			bagMap[parts[0]] = bagList
		}
	}

	// get # of bags the shiny gold bag contains (but don't count the shiny gold bag itself)
	answer = countBags("shiny gold") - 1

	// fmt.Println(bagMap)

	return answer
}

func countBags(outerBag string) (totalBags int) {
	bags := bagMap[outerBag]
	totalBags = 1

	if len(bags) != 0 {

		for _, bag := range bags {
			totalBags += bag.qty * countBags(bag.desc)
		}
	}
	return
}
