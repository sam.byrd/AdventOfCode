package aoc

import "fmt"

type position struct {
	x int
	y int
}

type tileType struct {
	tileID         int
	imgData        map[position]rune
	dimensions     position
	transformation int
}

func loadImages(input []string) (images map[int]tileType) {
	images = make(map[int]tileType)

	idx := 0
	bFirst := true
	row := 0
	tile := tileType{}

	for _, line := range input {
		if bFirst {
			bFirst = false
			images[idx] = tileType{}
			tile = images[idx]
			tile.imgData = make(map[position]rune)

			// grab the tileID
			fmt.Sscanf(line, "Tile %d:", &tile.tileID)

		} else if line == "" {
			// store the current image, start a new one
			tile.dimensions = position{x: row, y: row}
			images[idx] = tile
			bFirst = true
			idx++
			row = 0
		} else {
			for col, pixel := range line {
				tile.imgData[position{x: col, y: row}] = pixel
			}
			row++
		}
	}

	return images
}

func printTile(tile tileType) {
	fmt.Printf("Transformation: %d\n", tile.transformation)
	for row := 0; row < tile.dimensions.y; row++ {
		for col := 0; col < tile.dimensions.x; col++ {
			fmt.Printf("%c", tile.imgData[position{x: col, y: row}])
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")
}

type transType int
type edgeType int

const (
	CW90 transType = iota
	CCW90
	CW180
	FlipX
	FlipY
)

const (
	NS edgeType = iota
	SN
	EW
	WE
)

func transform(inTile tileType, transformation transType) (outTile tileType) {
	outTile = tileType{}
	outTile.imgData = make(map[position]rune)
	outTile.dimensions = inTile.dimensions
	outTile.tileID = inTile.tileID

	switch transformation {
	case CW90: // CW 90
		//x = y
		//y = flipX

		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{x: inTile.dimensions.y - row - 1, y: col}] = inTile.imgData[position{x: col, y: row}]
			}
		}

	case CCW90: // CCW 90
		// x = flipY
		// y = x
		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{y: inTile.dimensions.y - row - 1, x: inTile.dimensions.x - col - 1}] = inTile.imgData[position{x: col, y: row}]
			}
		}

		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				inTile.imgData[position{x: col, y: row}] = outTile.imgData[position{x: col, y: row}]
			}
		}

		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{x: inTile.dimensions.y - row - 1, y: col}] = inTile.imgData[position{x: col, y: row}]
			}
		}

	case CW180: // CW/CCW 180
		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{y: inTile.dimensions.y - row - 1, x: inTile.dimensions.x - col - 1}] = inTile.imgData[position{x: col, y: row}]
			}
		}

	case FlipX: // flipX
		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{y: row, x: inTile.dimensions.x - col - 1}] = inTile.imgData[position{x: col, y: row}]
			}
		}

	case FlipY: // flipY
		for row := 0; row < inTile.dimensions.y; row++ {
			for col := 0; col < inTile.dimensions.x; col++ {
				outTile.imgData[position{x: col, y: inTile.dimensions.y - row - 1}] = inTile.imgData[position{x: col, y: row}]
			}
		}

	default:
		panic("unknown transformation")
	}

	return
}

func compareEdge(tile1 tileType, tile2 tileType, edge edgeType) bool {
	// edge nums
	// num  tile1  tile2
	// 0    N      S
	// 1    S      N
	// 2    E      W
	// 3    W      E

	switch edge {
	case NS:
		for col := 0; col < tile1.dimensions.x; col++ {
			if tile1.imgData[position{y: 0, x: col}] != tile2.imgData[position{y: tile2.dimensions.y - 1, x: col}] {
				return false
			}
		}
		return true
	case SN:
		for col := 0; col < tile1.dimensions.x; col++ {
			if tile2.imgData[position{y: 0, x: col}] != tile1.imgData[position{y: tile1.dimensions.x - 1, x: col}] {
				return false
			}
		}
		return true
	case EW: //W-E
		for row := 0; row < tile1.dimensions.y; row++ {
			if tile1.imgData[position{y: row, x: tile2.dimensions.x - 1}] != tile2.imgData[position{y: row, x: 0}] {
				return false
			}
		}
		return true
	case WE: //E-W
		for row := 0; row < tile1.dimensions.y; row++ {
			if tile2.imgData[position{y: row, x: tile2.dimensions.x - 1}] != tile1.imgData[position{y: row, x: 0}] {
				return false
			}
		}
		return true
	}

	return false
}

// Day20Part1 is ...
func Day20Part1(data []string) (answer int) {
	image := make(map[position]int)
	tileToPos := make(map[int]position)

	tiles := loadImages(data)
	placedTiles := []int{}
	tilesToPlace := []int{}

	// put the 1st tile @ 0,0
	image[position{x: 0, y: 0}] = 0
	placedTiles = append(placedTiles, 0)
	tileToPos[0] = position{x: 0, y: 0}

	// load tilesToPlace
	for idx := 1; idx < len(tiles); idx++ {
		tilesToPlace = append(tilesToPlace, idx)
	}

	// fmt.Printf("Tile: %d\n", tiles[0].tileID)
	// printTile(tiles[0])
	for len(placedTiles) < len(tiles) {
		// loop over placed tiles, check NSEW, if empty, find tile
		for _, v := range placedTiles {

			//North
			newPos := position{x: tileToPos[v].x, y: tileToPos[v].y - 1}
			if _, ok := image[newPos]; !ok {
				// loop over tiles to place
				for toPlaceIdx, toPlace := range tilesToPlace {
					// try untransformed
					if compareEdge(tiles[v], tiles[toPlace], NS) {
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipX
					tempTile := transform(tiles[toPlace], FlipX)
					if compareEdge(tiles[v], tempTile, NS) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipY
					tempTile = transform(tiles[toPlace], FlipY)
					if compareEdge(tiles[v], tempTile, NS) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW90
					tempTile = transform(tiles[toPlace], CW90)
					if compareEdge(tiles[v], tempTile, NS) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW180
					tempTile = transform(tiles[toPlace], CW180)
					if compareEdge(tiles[v], tempTile, NS) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CCW90
					tempTile = transform(tiles[toPlace], CCW90)
					if compareEdge(tiles[v], tempTile, NS) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}
				}
			}

			//South
			newPos = position{x: tileToPos[v].x, y: tileToPos[v].y + 1}
			if _, ok := image[newPos]; !ok {
				// loop over tiles to place
				for toPlaceIdx, toPlace := range tilesToPlace {
					// try untransformed
					if compareEdge(tiles[v], tiles[toPlace], SN) {
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipX
					tempTile := transform(tiles[toPlace], FlipX)
					if compareEdge(tiles[v], tempTile, SN) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipY
					tempTile = transform(tiles[toPlace], FlipY)
					if compareEdge(tiles[v], tempTile, SN) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW90
					tempTile = transform(tiles[toPlace], CW90)
					if compareEdge(tiles[v], tempTile, SN) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW180
					tempTile = transform(tiles[toPlace], CW180)
					if compareEdge(tiles[v], tempTile, SN) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CCW90
					tempTile = transform(tiles[toPlace], CCW90)
					if compareEdge(tiles[v], tempTile, SN) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}
				}
			}

			//East
			newPos = position{x: tileToPos[v].x + 1, y: tileToPos[v].y}
			if _, ok := image[newPos]; !ok {
				// loop over tiles to place
				for toPlaceIdx, toPlace := range tilesToPlace {
					// try untransformed
					if compareEdge(tiles[v], tiles[toPlace], WE) {
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipX
					tempTile := transform(tiles[toPlace], FlipX)
					if compareEdge(tiles[v], tempTile, WE) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipY
					tempTile = transform(tiles[toPlace], FlipY)
					if compareEdge(tiles[v], tempTile, WE) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW90
					tempTile = transform(tiles[toPlace], CW90)
					if compareEdge(tiles[v], tempTile, WE) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW180
					tempTile = transform(tiles[toPlace], CW180)
					if compareEdge(tiles[v], tempTile, WE) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CCW90
					tempTile = transform(tiles[toPlace], CCW90)
					if compareEdge(tiles[v], tempTile, WE) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}
				}
			}

			//West
			newPos = position{x: tileToPos[v].x + 1, y: tileToPos[v].y}
			if _, ok := image[newPos]; !ok {
				// loop over tiles to place
				for toPlaceIdx, toPlace := range tilesToPlace {
					// try untransformed
					if compareEdge(tiles[v], tiles[toPlace], EW) {
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipX
					tempTile := transform(tiles[toPlace], FlipX)
					if compareEdge(tiles[v], tempTile, EW) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try FlipY
					tempTile = transform(tiles[toPlace], FlipY)
					if compareEdge(tiles[v], tempTile, EW) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW90
					tempTile = transform(tiles[toPlace], CW90)
					if compareEdge(tiles[v], tempTile, EW) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CW180
					tempTile = transform(tiles[toPlace], CW180)
					if compareEdge(tiles[v], tempTile, EW) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}

					// try CCW90
					tempTile = transform(tiles[toPlace], CCW90)
					if compareEdge(tiles[v], tempTile, EW) {
						tempTile.dimensions = tiles[toPlace].dimensions
						tempTile.tileID = tiles[toPlace].tileID
						tiles[toPlace] = tempTile
						image[newPos] = toPlace
						tileToPos[toPlace] = newPos
						placedTiles = append(placedTiles, toPlace)
						if toPlaceIdx == len(tilesToPlace) {
							tilesToPlace = tilesToPlace[:toPlaceIdx]
						} else if toPlaceIdx == 0 {
							tilesToPlace = tilesToPlace[toPlaceIdx+1:]
						} else {
							tilesToPlace = append(tilesToPlace[:toPlaceIdx], tilesToPlace[toPlaceIdx+1:]...)
						}
						break
					}
				}
			}
		}
	}

	// for idx := 1; idx < len(tiles) && len(placedTiles) < len(tiles); idx++ {
	// 	if !placedTiles[idx] {
	// 		// attempt to place
	// 		// for every placed tile, check NSEW
	// 		// if slot is empty, attempt to transform
	// 	}
	// 	tile := tiles[idx]
	// 	fmt.Printf("Tile: %d\n", idx)
	// 	printTile(tile)
	// 	fmt.Println("")
	// }

	return
}

// Day20Part2 is ...
func Day20Part2(data []string) (answer int) {

	return
}
