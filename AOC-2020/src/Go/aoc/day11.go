package aoc

func countAround(yard [102][102]byte, y int, x int) (numLit int) {
	numLit = 0

	if yard[y-1][x-1] == '#' {
		numLit++
	}

	if yard[y-1][x] == '#' {
		numLit++
	}

	if yard[y-1][x+1] == '#' {
		numLit++
	}

	if yard[y][x-1] == '#' {
		numLit++
	}

	if yard[y][x+1] == '#' {
		numLit++
	}

	if yard[y+1][x-1] == '#' {
		numLit++
	}

	if yard[y+1][x] == '#' {
		numLit++
	}

	if yard[y+1][x+1] == '#' {
		numLit++
	}

	return
}

func countVisibleAround(yard [102][102]byte, sizeY int, sizeX int, inY int, inX int) (numLit int) {
	numLit = 0
	maxX := sizeX + 1
	maxY := sizeY + 1
	bFound := false

	//NW
	y := inY - 1
	x := inX - 1
	for x >= 0 && y >= 0 && !bFound {
		if yard[y][x] == 'L' {
			bFound = true
		}
		if yard[y][x] == '#' {
			numLit++
			bFound = true
		}
		x--
		y--
	}
	bFound = false

	//N
	for y = inY - 1; y >= 0 && !bFound; y-- {
		if yard[y][inX] == 'L' {
			bFound = true
		}
		if yard[y][inX] == '#' {
			numLit++
			bFound = true
		}
	}
	bFound = false

	//NE
	y = inY - 1
	x = inX + 1
	for x <= maxX && y >= 0 && !bFound {
		if yard[y][x] == 'L' {
			bFound = true
		}
		if yard[y][x] == '#' {
			numLit++
			bFound = true
		}
		y--
		x++
	}
	bFound = false

	//E
	for x = inX + 1; x <= maxX && !bFound; x++ {
		if yard[inY][x] == 'L' {
			bFound = true
		}
		if yard[inY][x] == '#' {
			numLit++
			bFound = true
		}
	}
	bFound = false

	//SE
	y = inY + 1
	x = inX + 1
	for x <= maxX && y <= maxY && !bFound {
		if yard[y][x] == 'L' {
			bFound = true
		}
		if yard[y][x] == '#' {
			numLit++
			bFound = true
		}
		y++
		x++
	}
	bFound = false

	//S
	for y = inY + 1; y <= maxY && !bFound; y++ {
		if yard[y][inX] == 'L' {
			bFound = true
		}
		if yard[y][inX] == '#' {
			numLit++
			bFound = true
		}
	}
	bFound = false

	//SW
	y = inY + 1
	x = inX - 1
	for x >= 0 && y <= maxY && !bFound {
		if yard[y][x] == 'L' {
			bFound = true
		}
		if yard[y][x] == '#' {
			numLit++
			bFound = true
		}
		y++
		x--
	}
	bFound = false

	//W
	for x = inX - 1; x >= 0 && !bFound; x-- {
		if yard[inY][x] == 'L' {
			bFound = true
		}
		if yard[inY][x] == '#' {
			numLit++
			bFound = true
		}
	}

	return
}

func parseLine(yard *[102][102]byte, rowNum int, rowData string) {
	for x := 0; x < len(rowData); x++ {
		yard[rowNum][x+1] = rowData[x]
	}
}

// day11Part1 is ...
func day11Part1(data []string) (lightCount int) {
	var yard [102][102]byte
	var tempYard [102][102]byte

	// init yard
	for y := 0; y < 102; y++ {
		for x := 0; x < 102; x++ {
			yard[y][x] = '.'
			tempYard[y][x] = '.'
		}
	}

	// load initial image
	for i := 0; i < len(data); i++ {
		parseLine(&yard, i+1, data[i])
	}

	emptyCount := 0
	occupiedCount := 0
	for y := 0; y < 102; y++ {
		for x := 0; x < 102; x++ {
			if yard[y][x] == 'L' {
				emptyCount++
			} else if yard[y][x] == '#' {
				occupiedCount++
			}
		}
	}

	emptyCountOld := -1
	occupiedCountOld := -1

	// print status
	for y := 1; y <= len(data); y++ {
		for x := 1; x <= len(data[0]); x++ {
			// fmt.Printf("%c", yard[y][x])
		}
		// fmt.Printf("\n")
	}
	// fmt.Printf("\n")

	for emptyCount != emptyCountOld && occupiedCount != occupiedCountOld {
		emptyCountOld = emptyCount
		occupiedCountOld = occupiedCount

		for y := 1; y <= len(data); y++ {
			for x := 1; x <= len(data[0]); x++ {

				// count lights that are on
				count := countAround(yard, y, x)
				//				fmt.Printf("%d %d - %c", y, x, count)
				if yard[y][x] == 'L' && count == 0 {
					tempYard[y][x] = '#'
					occupiedCount++
					emptyCount--
				} else if yard[y][x] == '#' && count >= 4 {
					tempYard[y][x] = 'L'
					occupiedCount--
					emptyCount++
				} else {
					// do nothing
				}
			}
		}

		//copy back to yard
		for y := 1; y <= len(data); y++ {
			for x := 1; x <= len(data[0]); x++ {
				yard = tempYard
			}
		}

		// print status
		// for y := 1; y <= len(data); y++ {
		// 	for x := 1; x <= len(data[0]); x++ {
		// 		fmt.Printf("%c", yard[y][x])
		// 	}
		// 	fmt.Printf("\n")
		// }
		// fmt.Printf("\n")
	}

	// count # of lights on
	// for y := 1; y <= len(data); y++ {
	// 	for x := 1; x <= len(data[0]); x++ {
	// 		if yard[y][x] == '#' {
	// 			lightCount++
	// 		}
	// 	}
	// }

	return occupiedCount

}

// day11Part2 is ...
func day11Part2(data []string) (lightCount int) {
	var yard [102][102]byte
	var tempYard [102][102]byte

	// init yard
	for y := 0; y < 102; y++ {
		for x := 0; x < 102; x++ {
			yard[y][x] = '.'
			tempYard[y][x] = '.'
		}
	}

	// load initial image
	for i := 0; i < len(data); i++ {
		parseLine(&yard, i+1, data[i])
	}

	emptyCount := 0
	occupiedCount := 0
	for y := 1; y <= len(data); y++ {
		for x := 1; x <= len(data[0]); x++ {
			if yard[y][x] == 'L' {
				emptyCount++
			} else if yard[y][x] == '#' {
				occupiedCount++
			}
		}
	}

	emptyCountOld := -1
	occupiedCountOld := -1

	// print status
	for y := 1; y <= len(data); y++ {
		for x := 1; x <= len(data[0]); x++ {
			// fmt.Printf("\"%c\",", yard[y][x])
		}
		// fmt.Printf(" ")
		for x := 1; x <= len(data[0]); x++ {
			// fmt.Printf(",")
		}
		// fmt.Printf("\n")
	}
	// fmt.Printf("\n")

	for emptyCount != emptyCountOld && occupiedCount != occupiedCountOld {
		emptyCountOld = emptyCount
		occupiedCountOld = occupiedCount

		for y := 1; y <= len(data); y++ {
			for x := 1; x <= len(data[0]); x++ {
				if yard[y][x] == '.' {
					// fmt.Printf("\".\",")
				} else {
					// count lights that are on
					count := countVisibleAround(yard, len(data), len(data[0]), y, x)
					//				fmt.Printf("%d %d - %c", y, x, count)
					// fmt.Printf("%d,", count)
					if yard[y][x] == 'L' && count == 0 {
						tempYard[y][x] = '#'
						occupiedCount++
						emptyCount--
					} else if yard[y][x] == '#' && count >= 5 {
						tempYard[y][x] = 'L'
						occupiedCount--
						emptyCount++
					} else {
						// do nothing
					}
				}
			}
			// fmt.Printf(",")
			for x := 1; x <= len(data[0]); x++ {
				// fmt.Printf("\"%c\",", tempYard[y][x])
			}
			// fmt.Println("")
		}
		// fmt.Println("")

		//copy back to yard
		for y := 1; y <= len(data); y++ {
			for x := 1; x <= len(data[0]); x++ {
				yard = tempYard
			}
		}

		// print status
		// for y := 1; y <= len(data); y++ {
		// 	for x := 1; x <= len(data[0]); x++ {
		// 		fmt.Printf("%c", yard[y][x])
		// 	}
		// 	fmt.Printf("\n")
		// }
		// fmt.Printf("\n")
	}

	// count # of lights on
	// for y := 1; y <= count; y++ {
	// 	for x := 1; x <= count; x++ {
	// 		if yard[y][x] == '#' {
	// 			lightCount++
	// 		}
	// 	}
	// }

	return occupiedCount

}
