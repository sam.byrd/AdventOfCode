package aoc

import (
	"math"
	"sort"
	"strconv"
)

// Adapter is ...
type Adapter struct {
	val  int
	diff int
}

// day10Part1 is ...
func day10Part1(data []string) (answer int) {
	// plug is 0
	// diff to next must be 1, 2, or 3

	// convert to an int array
	dataInt := []int{}

	for _, v := range data {
		val, _ := strconv.Atoi(v)
		dataInt = append(dataInt, val)
	}

	// take input data, sort
	sort.Ints(dataInt)

	diff1 := 0
	diff3 := 0
	lastVal := 0

	for _, v := range dataInt {
		if v-lastVal == 1 {
			diff1++
		} else {
			diff3++
		}
		lastVal = v
	}
	diff3++

	return diff1 * diff3
}

func day10IsValid(adapterList []int, deviceInput int) (valid bool) {
	previous := 0

	for v := range adapterList {
		if v-previous < 1 || v-previous > 3 {
			return false
		}
		previous = v
	}

	previous = deviceInput - adapterList[len(adapterList)-1]
	if previous < 1 || previous > 3 {
		return false
	}

	return true
}

// day10Part2 is ...
func day10Part2(data []string) (answer float64) {
	adapterList := []int{}
	gapsize := float64(0)
	diff1 := float64(0)
	diff2 := float64(0)
	diff3 := float64(0)
	diff4 := float64(0)

	// seed with input
	for _, v := range data {
		val, _ := strconv.Atoi(v)
		adapterList = append(adapterList, val)
	}

	// take input data, sort
	sort.Ints(adapterList)

	// add device
	adapterList = append(adapterList, adapterList[len(adapterList)-1]+3)

	// count gaps
	previous := 0
	for _, v := range adapterList {
		if v-previous == 3 {
			switch gapsize {
			case 1:
				diff1++
			case 2:
				diff2++
			case 3:
				diff3++
			case 4:
				diff4++
			}
			gapsize = 0
		} else {
			gapsize++
		}
		previous = v
	}

	answer = math.Pow(1, diff1) * math.Pow(2, diff2) * math.Pow(4, diff3) * math.Pow(7, diff4)

	return
}
