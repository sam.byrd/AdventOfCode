package aoc

import (
	"fmt"
	"strings"
)

func day02solve1(data []string, count int) (answer int) {
	goodCount := 0
	minCount := 0
	maxCount := 0
	letter := ""
	password := ""

	//1-3 a: abcde
	for i := 0; i < len(data); i++ {
		fmt.Sscanf(data[i], "%d-%d %1s: %s", &minCount, &maxCount, &letter, &password)
		count := strings.Count(password, letter)
		if count >= minCount && count <= maxCount {
			goodCount++
		} else {
			//fmt.Printf("letter: %s min: %d, max: %d, found: %d => %s\n", letter, minCount, maxCount, count, password)
		}
	}

	return goodCount
}

func day02solve2(data []string, count int) (answer int) {
	goodCount := 0
	minCount := 0
	maxCount := 0
	letter := ""
	password := ""

	//1-3 a: abcde
	for i := 0; i < len(data); i++ {
		fmt.Sscanf(data[i], "%d-%d %1s: %s", &minCount, &maxCount, &letter, &password)
		if (password[minCount-1] == letter[0] && password[maxCount-1] != letter[0]) ||
			(password[minCount-1] != letter[0] && password[maxCount-1] == letter[0]) {
			goodCount++
		}
	}

	return goodCount
}
