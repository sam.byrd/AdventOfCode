package aoc

import (
	"fmt"
	"math"
)

// day12Part1 is ...
func day12Part1(data []string) (answer float64) {

	shipEW := 0
	shipNS := 0
	currDir := "E"

	for _, v := range data {
		instr := ""
		dist := 0

		fmt.Sscanf(v, "%1s%d", &instr, &dist)

		if instr == "F" {
			switch currDir {
			case "E":
				shipEW += dist
			case "W":
				shipEW -= dist
			case "N":
				shipNS += dist
			case "S":
				shipNS -= dist
			}
		} else if instr == "E" {
			shipEW += dist
		} else if instr == "W" {
			shipEW -= dist
		} else if instr == "S" {
			shipNS -= dist
		} else if instr == "N" {
			shipNS += dist
		} else if instr == "L" {
			if currDir == "N" {
				switch dist {
				case 90:
					currDir = "W"
				case 180:
					currDir = "S"
				case 270:
					currDir = "E"
				}
			} else if currDir == "S" {

				switch dist {
				case 90:
					currDir = "E"
				case 180:
					currDir = "N"
				case 270:
					currDir = "W"
				}
			} else if currDir == "W" {
				switch dist {
				case 90:
					currDir = "S"
				case 180:
					currDir = "E"
				case 270:
					currDir = "N"
				}
			} else if currDir == "E" {
				switch dist {
				case 90:
					currDir = "N"
				case 180:
					currDir = "W"
				case 270:
					currDir = "S"
				}
			}
		} else if instr == "R" {
			if currDir == "N" {
				switch dist {
				case 90:
					currDir = "E"
				case 180:
					currDir = "S"
				case 270:
					currDir = "W"
				}
			} else if currDir == "S" {

				switch dist {
				case 90:
					currDir = "W"
				case 180:
					currDir = "N"
				case 270:
					currDir = "E"
				}
			} else if currDir == "W" {
				switch dist {
				case 90:
					currDir = "N"
				case 180:
					currDir = "E"
				case 270:
					currDir = "S"
				}
			} else if currDir == "E" {
				switch dist {
				case 90:
					currDir = "S"
				case 180:
					currDir = "W"
				case 270:
					currDir = "N"
				}
			}
		}

	}
	return math.Abs(float64(shipEW)) + math.Abs(float64(shipNS))

}

// day12Part2 is ...
func day12Part2(data []string) (answer float64) {
	waypointEW := 10
	waypointNS := 1

	shipEW := 0
	shipNS := 0

	for _, v := range data {
		instr := ""
		dist := 0

		fmt.Sscanf(v, "%1s%d", &instr, &dist)

		if instr == "F" {
			shipEW += dist * waypointEW
			shipNS += dist * waypointNS

		} else if instr == "E" {
			waypointEW += dist
		} else if instr == "W" {
			waypointEW -= dist
		} else if instr == "S" {
			waypointNS -= dist
		} else if instr == "N" {
			waypointNS += dist
		} else if instr == "L" {
			switch dist {
			case 90:
				temp := waypointNS
				waypointNS = waypointEW
				waypointEW = -temp
			case 180:
				waypointNS = -waypointNS
				waypointEW = -waypointEW
			case 270:
				temp := waypointNS
				waypointNS = -waypointEW
				waypointEW = temp
			}
		} else if instr == "R" {
			switch dist {
			case 90:
				temp := waypointNS
				waypointNS = -waypointEW
				waypointEW = temp
			case 180:
				waypointNS = -waypointNS
				waypointEW = -waypointEW
			case 270:
				temp := waypointNS
				waypointNS = waypointEW
				waypointEW = -temp
			}
		}

	}
	return math.Abs(float64(shipEW)) + math.Abs(float64(shipNS))

}
