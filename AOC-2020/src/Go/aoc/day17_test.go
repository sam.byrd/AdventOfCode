package aoc

import (
	"testing"
)

func TestDay17Part1Example1(t *testing.T) {
	data := []string{
		".#.",
		"..#",
		"###",
	}

	answer := Day17Part1(data)

	if answer != 112 {
		t.Errorf("Testday17Part1Example1 gave %d, expected 112", answer)
	}
}

func TestDay17Part2Example1(t *testing.T) {
	data := []string{
		".#.",
		"..#",
		"###",
	}

	answer := Day17Part2(data, 6)

	if answer != 848 {
		t.Errorf("Testday17Part2Example1 gave %d, expected 848", answer)
	}
}

func TestDay17Part1(t *testing.T) {

	data, _ := loadData("../../../data/day17.dat")

	answer := Day17Part1(data)

	if answer != 424 {
		t.Errorf("TestDay17Part1 returned %d, expected 424", answer)
	}
}

func TestDay17Part2(t *testing.T) {

	data, _ := loadData("../../../data/day17.dat")

	answer := Day17Part2(data, 6)

	if answer != 424 {
		t.Errorf("TestDay17Part2 returned %d, expected 424", answer)
	}
}
