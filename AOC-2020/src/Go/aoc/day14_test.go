package aoc

import "testing"

func TestDay14Part1Example1(t *testing.T) {
	data := []string{
		"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
		"mem[8] = 11",
		"mem[7] = 101",
		"mem[8] = 0",
	}

	answer := day14Part1(data)

	if answer != 165 {
		t.Errorf("Testday14Example1 gave %d, expected 165", answer)
	}
}

func TestDay14Part2Example1(t *testing.T) {
	data := []string{
		"mask = 000000000000000000000000000000X1001X",
		"mem[42] = 100",
		"mask = 00000000000000000000000000000000X0XX",
		"mem[26] = 1",
	}

	answer := day14Part2(data)

	if answer != 208 {
		t.Errorf("TestDay14Part2Example1 gave %d, expected 208", answer)
	}
}

func TestDay14Part1(t *testing.T) {

	data, _ := loadData("../../../data/day14.dat")

	answer := day14Part1(data)

	if answer != 5875750429995 {
		t.Errorf("TestDay14Part1 returned %d, expected 5875750429995", answer)
	}
}

func TestDay14Part2(t *testing.T) {

	data, _ := loadData("../../../data/day14.dat")

	answer := day14Part2(data)

	if answer != 5272149590143 {
		t.Errorf("TestDay14Part2 returned %d, expected 5272149590143", answer)
	}
}
