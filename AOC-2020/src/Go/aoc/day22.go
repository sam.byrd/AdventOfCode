package aoc

import (
	"fmt"
	"strconv"
	"strings"
)

var debug = true
var lastGame = 0

type player struct {
	playerNum    int
	deck         []int
	previousDeck []string
}

func (p *player) String() string {
	var sb strings.Builder

	for _, v := range p.deck {
		if sb.Len() > 0 {
			sb.WriteString(",")
		}
		sb.WriteString(strconv.Itoa(v))
	}

	return sb.String()
}

func Day22ReadDecks(input []string) (player1 player, player2 player) {
	v := ""
	pos := 0
	for pos, v = range input {
		if strings.Contains(v, "Player 1") {
			continue
		} else if strings.Contains(v, "Player 2") {
			break
		}
		card, err := strconv.Atoi(v)
		if err == nil {
			player1.deck = append(player1.deck, card)
		}
	}

	for pos, v = range input[pos:] {
		if strings.Contains(v, "Player 2") {
			continue
		}
		card, err := strconv.Atoi(v)
		if err == nil {
			player2.deck = append(player2.deck, card)
		}
	}

	return
}

func Day22Part1(input []string) (result int) {
	var player1 player
	var player2 player

	player1.playerNum = 1
	player2.playerNum = 2

	player1, player2 = Day22ReadDecks(input)

	fmt.Println("Player 1: ", player1)
	fmt.Println("Player 2: ", player2)

	round := 1
	for len(player1.deck) > 0 && len(player2.deck) > 0 {
		fmt.Println("-- Round ", round, " --")
		fmt.Println("Player 1's deck: ", player1.deck)
		fmt.Println("Player 2's deck: ", player2.deck)
		fmt.Println("Player 1 plays: ", player1.deck[0])
		fmt.Println("Player 2 plays: ", player2.deck[0])
		if player1.deck[0] > player2.deck[0] {
			fmt.Println("Player 1 wins the round!")
			player1.deck = append(player1.deck, player1.deck[0])
			player1.deck = append(player1.deck, player2.deck[0])
			player1.deck = player1.deck[1:]
			player2.deck = player2.deck[1:]
		} else {
			fmt.Println("Player 2 wins the round!")
			player2.deck = append(player2.deck, player2.deck[0])
			player2.deck = append(player2.deck, player1.deck[0])
			player1.deck = player1.deck[1:]
			player2.deck = player2.deck[1:]

		}
		round++
	}

	fmt.Println("== Post-game results ==")

	if len(player1.deck) > 0 {
		result = 0
		for idx, card := range player1.deck {
			result += card * (len(player1.deck) - idx)
		}
	} else {
		result = 0
		for idx, card := range player2.deck {
			result += card * (len(player2.deck) - idx)
			fmt.Println(card, " * ", len(player2.deck)-idx)
		}
	}

	return
}

func matchesPreviousDeck(player1 player, player2 player) bool {
	// fmt.Printf("  Checking player %d for dups", playerX.playerNum)
	for i := 0; i < len(player1.previousDeck); i++ {
		if player1.String() == player1.previousDeck[i] && player2.String() == player2.previousDeck[i] {
			return true
		}
	}
	return false
}

func Day22Game2(gameNum int, player1 player, player2 player) (winner int, winningDeck []int) {
	lastGame = gameNum
	winner = 0
	if debug {
		fmt.Printf("=== Game %d ===\n\n", gameNum)
	}

	round := 1
	for len(player1.deck) > 0 && len(player2.deck) > 0 {
		if round > 10000 {
			panic("Perhaps we're in a loop?")
		}
		if debug {
			fmt.Printf("-- Round %d (Game %d) --\n", round, gameNum)
			fmt.Println("Player 1's deck: ", player1.deck)
			fmt.Println("Player 2's deck: ", player2.deck)
			fmt.Println("Player 1 plays: ", player1.deck[0])
			fmt.Println("Player 2 plays: ", player2.deck[0])
		}
		if matchesPreviousDeck(player1, player2) {
			winner = 1
			winningDeck = player1.deck
			return
		} else if len(player1.deck)-1 >= player1.deck[0] && len(player2.deck)-1 >= player2.deck[0] {
			// subgame time
			if debug {
				fmt.Println("Playing a sub-game to determine the winner...")
				fmt.Println("")
			}

			player1b := player{playerNum: 1}
			player2b := player{playerNum: 2}
			player1b.deck = append(player1b.deck, player1.deck[1:player1.deck[0]+1]...)
			player2b.deck = append(player2b.deck, player2.deck[1:player2.deck[0]+1]...)
			winner, _ = Day22Game2(lastGame+1, player1b, player2b)

			if debug {
				fmt.Printf("...anyway, back to game %d.\n\n", gameNum)
			}
		} else if player1.deck[0] > player2.deck[0] {
			winner = 1
		} else {
			winner = 2
		}

		player1.previousDeck = append(player1.previousDeck, player1.String())
		player2.previousDeck = append(player2.previousDeck, player2.String())
		if debug {
			fmt.Printf("Player %d wins round %d of game %d\n\n", winner, round, gameNum)
		}
		if winner == 1 {
			player1.deck = append(player1.deck, player1.deck[0])
			player1.deck = append(player1.deck, player2.deck[0])
			player1.deck = player1.deck[1:]
			player2.deck = player2.deck[1:]
		} else {
			player2.deck = append(player2.deck, player2.deck[0])
			player2.deck = append(player2.deck, player1.deck[0])
			player1.deck = player1.deck[1:]
			player2.deck = player2.deck[1:]
		}

		round++
	}

	if len(player1.deck) > 0 {
		winner = 1
		winningDeck = player1.deck
	} else {
		winner = 2
		winningDeck = player2.deck
	}

	if debug {
		fmt.Printf("The winner of game %d is player %d!\n\n", gameNum, winner)
	}

	return
}

func Day22Part2(input []string) (result int) {
	var player1 player
	var player2 player

	player1, player2 = Day22ReadDecks(input)

	_, winningDeck := Day22Game2(1, player1, player2)

	fmt.Println("== Post-game results ==")

	result = 0
	for idx, card := range winningDeck {
		result += card * (len(winningDeck) - idx)
		fmt.Println(card, " * ", len(winningDeck)-idx)
	}

	return
}
