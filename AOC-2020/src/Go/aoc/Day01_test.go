package aoc

import (
	"testing"
)

func TestDay01Example1(t *testing.T) {
	data := []string{"1721", "979", "366", "299", "675", "1456"}
	answer := day01solve1(data, 6, 2020)

	if answer != 514579 {
		t.Errorf("Answer found was %d, expected was 514579", answer)
	}
}

func TestDay01Example2(t *testing.T) {
	data := []string{"1721", "979", "366", "299", "675", "1456"}
	answer := day01solve2(data, 6, 2020)

	if answer != 241861950 {
		t.Errorf("Answer found was %d, expected was 241861950", answer)
	}
}

func TestDay01Part1(t *testing.T) {
	data, count := loadData("../../../data/day01.dat")

	answer := day01solve1(data, count, 2020)

	if answer != 1005459 {
		t.Errorf("Answer found was %d, expected was 1005459", answer)
	}
}

func TestDay01Part2(t *testing.T) {
	data, count := loadData("../../../data/day01.dat")

	answer := day01solve2(data, count, 2020)

	if answer != 92643264 {
		t.Errorf("Answer found was %d, expected was 92643264", answer)
	}
}
