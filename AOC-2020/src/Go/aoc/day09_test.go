package aoc

import "testing"

func TestDay09Example1(t *testing.T) {
	data := []string{
		"35",
		"20",
		"15",
		"25",
		"47",
		"40",
		"62",
		"55",
		"65",
		"95",
		"102",
		"117",
		"150",
		"182",
		"127",
		"219",
		"299",
		"277",
		"309",
		"576",
	}

	answer := day09Part1(data, 5)

	if answer != 127 {
		t.Errorf("Testday09Example1 gave %d, expected 127", answer)
	}
}

func TestDay09Example2(t *testing.T) {
	data := []string{
		"35",
		"20",
		"15",
		"25",
		"47",
		"40",
		"62",
		"55",
		"65",
		"95",
		"102",
		"117",
		"150",
		"182",
		"127",
		"219",
		"299",
		"277",
		"309",
		"576",
	}

	answer := day09Part2(data, day09Part1(data, 5))

	if answer != 62 {
		t.Errorf("Testday09Example2 gave %d, expected 62", answer)
	}
}

func TestDay09Part1(t *testing.T) {

	data, _ := loadData("../../../data/day09.dat")

	answer := day09Part1(data, 25)

	if answer != 373803594 {
		t.Errorf("Test08Part1 returned %d, expected 1087", answer)
	}
}

func TestDay09Part2(t *testing.T) {

	data, _ := loadData("../../../data/day09.dat")

	answer := day09Part2(data, day09Part1(data, 25))

	if answer != 51152360 {
		t.Errorf("Test08Part2 returned %d, expected 51152360", answer)
	}
}
