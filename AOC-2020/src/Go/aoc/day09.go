package aoc

import "strconv"

func day09Part1(data []string, preambleLen int) (answer int) {
	var dataInt = []int{}
	count := len(data)
	for i := 0; i < count; i++ {
		num, _ := strconv.Atoi(data[i])
		dataInt = append(dataInt, num)
	}

	for i := preambleLen; i < count; i++ {
		if !isValid(dataInt[i-preambleLen:i], preambleLen, dataInt[i]) {
			answer = dataInt[i]
			break
		}
	}

	return
}

func isValid(data []int, count int, target int) (answer bool) {
	answer = false

	for i := 0; i < count; i++ {
		//num1, _ := strconv(data[i])
		for j := 1; j < count; j++ {
			if i != j {
				//num2, _ := strconv(data[j])
				if data[i]+data[j] == target {
					answer = true
					break
				}
			}
		}
	}

	return
}

func sumRange(data []int) (answer int) {
	for _, v := range data {
		answer += v
	}

	return
}

func day09Part2(data []string, target int) (answer int) {
	// find a contiguous set of numbers that add up to the target
	// return sum of min and max
	var dataInt = []int{}
	count := len(data)
	for i := 0; i < count; i++ {
		num, _ := strconv.Atoi(data[i])
		dataInt = append(dataInt, num)
	}

	i := 0
	j := 1

	for i < count && j <= count && answer != target {
		answer = sumRange(dataInt[i:j])
		if answer < target {
			j++
		} else if answer > target {
			i++
		} else {
			break
		}
	}

	// found the right range, now need to find min & max
	min := 99999999999999
	max := -1
	for _, v := range dataInt[i:j] {
		if v < min {
			min = v
		}

		if v > max {
			max = v
		}
	}

	answer = min + max

	return
}
