package aoc

import "testing"

func TestDay08Example1(t *testing.T) {
	data := []string{
		"nop +0",
		"acc +1",
		"jmp +4",
		"acc +3",
		"jmp -3",
		"acc -99",
		"acc +1",
		"jmp -4",
		"acc +6",
	}

	_, answer := day08Part1(data, len(data))

	if answer != 5 {
		t.Errorf("Testday08Example1 gave %d, expected 5", answer)
	}
}

func TestDay08Example2(t *testing.T) {
	data := []string{
		"nop +0",
		"acc +1",
		"jmp +4",
		"acc +3",
		"jmp -3",
		"acc -99",
		"acc +1",
		"jmp -4",
		"acc +6",
	}

	answer := day08Part2(data, len(data))

	if answer != 8 {
		t.Errorf("Testday08Example2 gave %d, expected 8", answer)
	}
}

func TestDay08Part1(t *testing.T) {

	data, count := loadData("../../../data/day08.dat")

	_, answer := day08Part1(data, count)

	if answer != 1087 {
		t.Errorf("Test08Part1 returned %d, expected 1087", answer)
	}
}

func TestDay08Part2(t *testing.T) {

	data, count := loadData("../../../data/day08.dat")

	answer := day08Part2(data, count)

	if answer != 780 {
		t.Errorf("Test08Part2 returned %d, expected 780", answer)
	}
}
