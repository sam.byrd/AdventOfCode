package aoc

import (
	"strings"
	"testing"
)

func TestDay22Part1Example1(t *testing.T) {
	data := `Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`

	answer := Day22Part1(strings.Split(data, "\n"))

	if answer != 306 {
		t.Errorf("TestDay22Part1Example1 gave %d, expected 306", answer)
	}
}

func TestDay22Part2Example2(t *testing.T) {
	data := `Player 1:
43
19

Player 2:
2
29
14`

	answer := Day22Part2(strings.Split(data, "\n"))

	if answer != 291 {
		t.Errorf("TestDay22Part2Example2 gave %d, expected 291", answer)
	}
}

func TestDay22Part2Example1(t *testing.T) {
	data := `Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`

	answer := Day22Part2(strings.Split(data, "\n"))

	if answer != 291 {
		t.Errorf("TestDay22Part2Example1 gave %d, expected 291", answer)
	}
}

func TestDay22RecursionMatch(t *testing.T) {
	data := `Player 1:
34
33
43
30
3
35

Player 2:
40`

	answer := Day22Part2(strings.Split(data, "\n"))

	if answer != 291 {
		t.Errorf("TestDay22RecursionMatch gave %d, expected 291", answer)
	}
}

// func TestDay22MatchPrevious(t *testing.T) {
// 	deck1 := []int{1, 2, 3}
// 	deck2 := []int{4, 5, 6, 7}

// 	player1 := player{playerNum: 1}
// 	player1.deck = append(player1.deck, deck1...)
// 	player1.previousDeck = append(player1.previousDeck, deck1)

// 	player2 := player{playerNum: 2}
// 	player2.deck = append(player2.deck, deck2...)
// 	player2.previousDeck = append(player2.previousDeck, deck2)

// 	if matchesPreviousDeck(player1, player2) != true {
// 		t.Errorf("TestDay22MatchPrevious gave false, expected true")
// 	}
// }

func TestDay22Part1(t *testing.T) {

	data, _ := loadData("../../../data/day22.dat")

	answer := Day22Part1(data)

	if answer != 31314 {
		t.Errorf("TestDay22Part1 returned %d, expected 31314", answer)
	}
}

func TestDay22Part2(t *testing.T) {

	data, _ := loadData("../../../data/day22.dat")

	answer := Day22Part2(data)

	if answer != 32760 {
		t.Errorf("TestDay22Part1 returned %d, expected 32760", answer)
	}
}
