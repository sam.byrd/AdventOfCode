package aoc

import (
	"strconv"
	"strings"
)

// day08Part1 is ...
func day08Part1(data []string, count int) (i int, accum int) {
	instrList := make(map[int]bool)

	i = 0
	for i < count {
		instr := instrList[i]
		if instr == false {
			instrList[i] = true
			parts := strings.Split(data[i], " ")
			if parts[0] == "nop" {
				// do nothing
				i++
			} else if parts[0] == "acc" {
				val, _ := strconv.Atoi(parts[1])
				accum += val
				i++
			} else if parts[0] == "jmp" {
				val, _ := strconv.Atoi(parts[1])
				i += val
			}
		} else {
			break
		}
	}

	return
}

func day08Part2(data []string, count int) (accum int) {

	i := 0
	lasti := 0
	for i < count {
		parts := strings.Split(data[i], " ")
		if parts[0] == "nop" {
			data[i] = "jmp " + parts[1]
		} else if parts[0] == "jmp" {
			data[i] = "nop " + parts[1]
		}
		lasti, accum = day08Part1(data, count)

		if lasti == count {
			break
		} else {
			if parts[0] == "nop" {
				data[i] = "nop " + parts[1]
			} else if parts[0] == "jmp" {
				data[i] = "jmp " + parts[1]
			}
		}
		i++
	}

	return accum
}
