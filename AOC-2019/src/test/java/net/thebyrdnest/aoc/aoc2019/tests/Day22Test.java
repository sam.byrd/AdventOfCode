package net.thebyrdnest.aoc.aoc2019.tests;

import net.thebyrdnest.aoc.aoc2019.Day22;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day22Test {


    @BeforeAll
    public static void setupBeforeClass() throws Exception {

    }

    @AfterAll
    public static void teardownAfterClass() throws Exception {

    }

    @BeforeEach
    public void setupBeforeEach() throws Exception {

    }

    @AfterEach
    public void tearDownAfterEach() throws Exception {

    }

    @Test
    public void Example1_dealNew() {
        Day22 day22 = new Day22();
        Map<Long, Long> newDeck = day22.dealNewDeck(day22.buildDeck(10));

        Map<Long, Long> expectedDeck = new HashMap<>();
        long card = 9;
        for (long i=0; i < 10; i++)
            expectedDeck.put(i, card--);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example2_cut() {
        Day22 day22 = new Day22();
        List<Integer> originalDeck = new ArrayList<>();
        for (int i=0; i<10; i++)
            originalDeck.add(i);

        Map<Long,Long> newDeck = day22.cutDeck(day22.buildDeck(10), 3);

        Map<Long,Long> expectedDeck = new HashMap<>();
        expectedDeck.put(0l,3l);
        expectedDeck.put(1l,4l);
        expectedDeck.put(2l,5l);
        expectedDeck.put(3l,6l);
        expectedDeck.put(4l,7l);
        expectedDeck.put(5l,8l);
        expectedDeck.put(6l,9l);
        expectedDeck.put(7l,0l);
        expectedDeck.put(8l,1l);
        expectedDeck.put(9l,2l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example3_dealN() {
        Day22 day22 = new Day22();
        Map<Long,Long> newDeck = day22.dealWithIncrement(day22.buildDeck(10), 3);

        Map<Long,Long> expectedDeck = new HashMap<>();
        expectedDeck.put(0l,0l);
        expectedDeck.put(1l,7l);
        expectedDeck.put(2l,4l);
        expectedDeck.put(3l,1l);
        expectedDeck.put(4l,8l);
        expectedDeck.put(5l,5l);
        expectedDeck.put(6l,2l);
        expectedDeck.put(7l,9l);
        expectedDeck.put(8l,6l);
        expectedDeck.put(9l,3l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example4_cut() {
        Day22 day22 = new Day22();

        String[] script = {"cut 3"};

        Map<Long,Long> newDeck = day22.solve(10, script);

        Map<Long,Long> expectedDeck = new HashMap<>();
        expectedDeck.put(0l,3l);
        expectedDeck.put(1l,4l);
        expectedDeck.put(2l,5l);
        expectedDeck.put(3l,6l);
        expectedDeck.put(4l,7l);
        expectedDeck.put(5l,8l);
        expectedDeck.put(6l,9l);
        expectedDeck.put(7l,0l);
        expectedDeck.put(8l,1l);
        expectedDeck.put(9l,2l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example4_cut_neg() {
        Day22 day22 = new Day22();

        String[] script = {"cut -4"};

        Map<Long,Long> newDeck = day22.solve(10, script);

        Map<Long,Long> expectedDeck = new HashMap<>();

        expectedDeck.put(0l,6l);
        expectedDeck.put(1l,7l);
        expectedDeck.put(2l,8l);
        expectedDeck.put(3l,9l);
        expectedDeck.put(4l,0l);
        expectedDeck.put(5l,1l);
        expectedDeck.put(6l,2l);
        expectedDeck.put(7l,3l);
        expectedDeck.put(8l,4l);
        expectedDeck.put(9l,5l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example4_dealN() {
        Day22 day22 = new Day22();
        List<Integer> originalDeck = new ArrayList<>();
        for (int i=0; i<10; i++)
            originalDeck.add(i);

        String[] script = {"deal with increment 3"};

        Map<Long,Long> newDeck = day22.solve(10, script);

        Map<Long,Long> expectedDeck = new HashMap<>();

        expectedDeck.put(0l,0l);
        expectedDeck.put(1l,7l);
        expectedDeck.put(2l,4l);
        expectedDeck.put(3l,1l);
        expectedDeck.put(4l,8l);
        expectedDeck.put(5l,5l);
        expectedDeck.put(6l,2l);
        expectedDeck.put(7l,9l);
        expectedDeck.put(8l,6l);
        expectedDeck.put(9l,3l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example4_dealNew() {
        Day22 day22 = new Day22();

        String[] script = {"deal into new stack"};

        Map<Long,Long> newDeck = day22.solve(10, script);

        Map<Long,Long> expectedDeck = new HashMap<>();

        expectedDeck.put(0l,9l);
        expectedDeck.put(1l,8l);
        expectedDeck.put(2l,7l);
        expectedDeck.put(3l,6l);
        expectedDeck.put(4l,5l);
        expectedDeck.put(5l,4l);
        expectedDeck.put(6l,3l);
        expectedDeck.put(7l,2l);
        expectedDeck.put(8l,1l);
        expectedDeck.put(9l,0l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example4_cut_zero() {
        Day22 day22 = new Day22();
        List<Integer> originalDeck = new ArrayList<>();
        for (int i=0; i<10; i++)
            originalDeck.add(i);

        String[] script = {"cut 0"};

        Map<Long,Long> newDeck = day22.solve(10, script);

        Map<Long,Long> expectedDeck = new HashMap<>();

        expectedDeck.put(0l,0l);
        expectedDeck.put(1l,1l);
        expectedDeck.put(2l,2l);
        expectedDeck.put(3l,3l);
        expectedDeck.put(4l,4l);
        expectedDeck.put(5l,5l);
        expectedDeck.put(6l,6l);
        expectedDeck.put(7l,7l);
        expectedDeck.put(8l,8l);
        expectedDeck.put(9l,9l);

        Assertions.assertEquals(expectedDeck, newDeck);
    }

    @Test
    public void Example5() {
        Day22 day22 = new Day22();
        String[] testInput = {"deal into new stack", "deal into new stack"};

        Map<Long, Long> expectedDeck = day22.buildDeck(100);

        Map<Long,Long> solution = day22.solve(100, testInput);

        Assertions.assertEquals(expectedDeck, solution);

    }

    @Test
    public void Problem1() {
        Day22 day22 = new Day22();

        Map<Long,Long> solution = day22.solve(10007, myInput);

        long index = 0;
        for (Long card : solution.values())
            if (card == 2019)
                break;
            else
                index++;

        Assertions.assertEquals(1538, index);
    }

    @Disabled
    @Test
    public void Problem2() {
        Day22 day22 = new Day22();

        // this unfortunately blows up HashMap
        //Map<Long,Long> solution = day22.solve2(119315717514047L,101741582076661L, myInput);
        Map<Long,Long> solution = day22.solve2(119315717514047L,1L, myInput);

        Assertions.assertEquals(96196710942473L, solution.get(2020));
    }

    String[] myInput = ("cut 4913\n" +
            "deal with increment 53\n" +
            "cut -1034\n" +
            "deal into new stack\n" +
            "deal with increment 49\n" +
            "deal into new stack\n" +
            "cut -7153\n" +
            "deal with increment 59\n" +
            "cut 4618\n" +
            "deal with increment 15\n" +
            "cut -8047\n" +
            "deal with increment 67\n" +
            "cut 9890\n" +
            "deal with increment 16\n" +
            "cut -870\n" +
            "deal with increment 34\n" +
            "cut -4557\n" +
            "deal with increment 19\n" +
            "cut -6466\n" +
            "deal with increment 47\n" +
            "cut -5860\n" +
            "deal with increment 65\n" +
            "deal into new stack\n" +
            "cut -8104\n" +
            "deal with increment 15\n" +
            "cut -9013\n" +
            "deal into new stack\n" +
            "cut 7309\n" +
            "deal with increment 36\n" +
            "deal into new stack\n" +
            "cut -1340\n" +
            "deal with increment 42\n" +
            "cut -5204\n" +
            "deal with increment 75\n" +
            "deal into new stack\n" +
            "deal with increment 16\n" +
            "deal into new stack\n" +
            "deal with increment 44\n" +
            "cut 6833\n" +
            "deal with increment 14\n" +
            "deal into new stack\n" +
            "cut 2345\n" +
            "deal with increment 60\n" +
            "cut 4830\n" +
            "deal with increment 75\n" +
            "cut -2843\n" +
            "deal with increment 50\n" +
            "cut 3816\n" +
            "deal into new stack\n" +
            "cut 7340\n" +
            "deal with increment 48\n" +
            "cut -3452\n" +
            "deal with increment 62\n" +
            "cut -2433\n" +
            "deal with increment 59\n" +
            "cut -4176\n" +
            "deal into new stack\n" +
            "cut 9365\n" +
            "deal with increment 65\n" +
            "cut -7815\n" +
            "deal with increment 65\n" +
            "cut 5177\n" +
            "deal into new stack\n" +
            "deal with increment 8\n" +
            "cut -3200\n" +
            "deal with increment 63\n" +
            "cut -9460\n" +
            "deal with increment 20\n" +
            "cut -6926\n" +
            "deal with increment 4\n" +
            "cut -9863\n" +
            "deal with increment 38\n" +
            "cut -4295\n" +
            "deal with increment 31\n" +
            "deal into new stack\n" +
            "cut -9590\n" +
            "deal with increment 22\n" +
            "cut -9805\n" +
            "deal with increment 48\n" +
            "deal into new stack\n" +
            "cut -2326\n" +
            "deal with increment 72\n" +
            "deal into new stack\n" +
            "deal with increment 7\n" +
            "cut -453\n" +
            "deal with increment 15\n" +
            "cut -7639\n" +
            "deal into new stack\n" +
            "deal with increment 61\n" +
            "cut -7303\n" +
            "deal with increment 24\n" +
            "cut 4827\n" +
            "deal with increment 48\n" +
            "cut -1821\n" +
            "deal with increment 31\n" +
            "cut -9410\n" +
            "deal with increment 5\n" +
            "cut -2492\n" +
            "deal with increment 25\n" +
            "cut 1313").split("\n");
}
