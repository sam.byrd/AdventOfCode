package net.thebyrdnest.aoc.aoc2019;

import java.util.*;

public class Day22 {

    public Map<Long, Long> buildDeck(long deckSize) {
        Map<Long, Long> deck = new HashMap<>();
        for (long index = 0; index < deckSize; index++) {
            deck.put(index, index);
        }

        return deck;
    }

    public Map<Long,Long> dealNewDeck(Map<Long,Long> oldDeck) {
        Map<Long,Long> newDeck = new HashMap<>();
        long lCardNum  = 0;
        for (long i=oldDeck.size()-1; i >= 0; i--) {
            newDeck.put(lCardNum++, oldDeck.get(i));
        }

        return newDeck;
    }

    public Map<Long, Long> cutDeck(Map<Long,Long> oldDeck, long position) {
        Map<Long, Long> newDeck = new HashMap<>();

        if (position < 0) {
            position = oldDeck.size() + position;
        }
        long newIndex = 0;

        for (long index = position; index < oldDeck.size(); index++) {
            newDeck.put(newIndex, oldDeck.get(index));
            newIndex++;
        }

        for (long index = 0; index < position; index++) {
            newDeck.put(newIndex, oldDeck.get(index));
            newIndex++;
        }

        return newDeck;
    }

    public Map<Long,Long> dealWithIncrement(Map<Long,Long> oldDeck, long increment) {
        Map<Long, Long> table = new HashMap<>();

        long iPos = 0;


        for (Map.Entry<Long,Long> card : oldDeck.entrySet()) {
            table.put(iPos, card.getValue());
            iPos = (iPos + increment) % oldDeck.size();
        }
        return table;
    }

    public Map<Long,Long> solve(long deckSize, String[] script) {
        Map<Long,Long> deck = buildDeck(deckSize);

        for (String instruction : script) {
            String[] instructionParts = instruction.split(" ");

            if (instructionParts[0].equalsIgnoreCase("cut")) {
                deck = cutDeck(deck, Long.parseLong(instructionParts[1]));
            } else if (instructionParts[2].equalsIgnoreCase("new")) {
                deck = dealNewDeck(deck);
            } else if (instructionParts[2].equalsIgnoreCase("increment")) {
                deck = dealWithIncrement(deck, Long.parseLong(instructionParts[3]));
            } else {
                System.err.println("some error");
            }
        }

        return deck;
    }

    public Map<Long,Long> solve2(long deckSize, long loops, String[] script) {
        Map<Long,Long> deck = buildDeck(deckSize);

        for (long iLoop = 0; iLoop < loops; iLoop++) {
            for (String instruction : script) {
                String[] instructionParts = instruction.split(" ");

                if (instructionParts[0].equalsIgnoreCase("cut")) {
                    deck = cutDeck(deck, Long.parseLong(instructionParts[1]));
                } else if (instructionParts[2].equalsIgnoreCase("new")) {
                    deck = dealNewDeck(deck);
                } else if (instructionParts[2].equalsIgnoreCase("increment")) {
                    deck = dealWithIncrement(deck, Long.parseLong(instructionParts[3]));
                } else {
                    System.err.println("some error");
                }
            }
        }

        return deck;
    }
}