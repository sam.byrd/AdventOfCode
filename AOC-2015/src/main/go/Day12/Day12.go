package day12

import "encoding/json"

//parse out into objects?
// Day12Part2Target is ...
func Day12Part2Target(contents *[]byte) float64 {
	input := *contents
	var f interface{}
	var output float64
	json.Unmarshal(input, &f)

	output = rec(f)

	return output
}

func rec(f interface{}) (output float64) {
outer:
	switch fv := f.(type) {
	case []interface{}:
		for _, val := range fv {
			output += rec(val)
		}
	case float64:
		output += fv
	case map[string]interface{}:
		for _, val := range fv {
			if val == "red" {
				break outer
			}
		}
		for _, val := range fv {
			output += rec(val)
		}
	}

	return output
}
