package day12

import (
	"io/ioutil"
	"testing"
)

// func loadData() (data Message, count int) {
// 	file, err := os.Open("../../../../data/day12.dat")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer file.Close()

// 	dec := json.NewDecoder(file)

// 	scanner := bufio.NewScanner(file)
// 	b := []byte(scanner.Text())
// 	err := json.Unmarshal(b, &data)

// 	return
// }

func TestDay12Part2Target(t *testing.T) {
	contents, _ := ioutil.ReadFile("../../../../data/day12.dat")

	answer := Day12Part2Target(&contents)

	if answer != 68466 {
		t.Errorf("TestDay12Part2 returned %f, expected 68466" answer)
	}
}
