package aoc

import "testing"

func TestDay22Part1Example1(t *testing.T) {
	data := []string{
		"inc a",
		"jio a, +2",
		"tpl a",
		"inc a",
	}

	answer := Day22Part1(data)

	if answer != 0 {
		t.Errorf("Day22Part1Example1 returned %d, expected 0", answer)
	}
}

func TestDay22Part1(t *testing.T) {
	data, _ := loadData("../../../../data/day22.dat")

	spent := Day22Part1(data)

	if spent != 170 {
		t.Errorf("Day22Part1 returned %d, expected 170", spent)
	}
}

func TestDay22Part2(t *testing.T) {
	data, _ := loadData("../../../../data/day22.dat")

	spent := Day22Part2(data)

	if spent != 247 {
		t.Errorf("Day22Part2 returned %d, expected 247", spent)
	}
}
