package aoc

import (
	"testing"
)

func TestDay20Example1(t *testing.T) {
	houseNum := Day20Part1(1, 150)

	if houseNum != 8 {
		t.Errorf("TestDay20Example1 returned %d, but we expected 8", houseNum)
	}
}

func TestDay20Part1(t *testing.T) {
	houseNum := Day20Part1(800000, 36000000)

	if houseNum != 831600 {
		t.Errorf("TestDay20Example1 returned %d, but we expected 831600", houseNum)
	}
}

func TestDay20Part2(t *testing.T) {
	//637000
	//houseNum := Day20Part1(830000, 36000000)

	min := 36000000
	houseNum := 1
	for Day20Part2_2(houseNum) < min {
		houseNum++
	}

	if houseNum != 8 {
		t.Errorf("TestDay20Example1 returned %d, but we expected 8", houseNum)
	}
}
