package aoc

import (
	"bufio"
	"log"
	"os"
)

func loadData(filename string) (data []string, count int) {

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	count = 0

	// load the input array
	for scanner.Scan() {
		if count > len(data) {
			break
		}
		data = append(data, scanner.Text())
		count++
	}

	return
}
