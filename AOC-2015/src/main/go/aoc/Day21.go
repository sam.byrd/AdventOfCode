package aoc

type Item struct {
	name   string
	cost   int
	damage int
	armor  int
}

var weapons = []Item{
	Item{
		name:   "Dagger",
		cost:   8,
		damage: 4,
		armor:  0,
	},
	Item{
		name:   "Shortsword",
		cost:   10,
		damage: 5,
		armor:  0,
	},
	Item{
		name:   "Warhammer",
		cost:   25,
		damage: 6,
		armor:  0,
	},
	Item{
		name:   "Longsword",
		cost:   40,
		damage: 7,
		armor:  0,
	},
	Item{
		name:   "Greataxe",
		cost:   74,
		damage: 8,
		armor:  0,
	},
}

var armor = []Item{
	Item{
		name:   "none",
		cost:   0,
		damage: 0,
		armor:  0,
	},
	Item{
		name:   "Leather",
		cost:   13,
		damage: 0,
		armor:  1,
	},
	Item{
		name:   "Chainmail",
		cost:   31,
		damage: 0,
		armor:  2,
	},
	Item{
		name:   "Spintmail",
		cost:   53,
		damage: 0,
		armor:  3,
	},
	Item{
		name:   "Bandedmail",
		cost:   75,
		damage: 0,
		armor:  4,
	},
	Item{
		name:   "Platemail",
		cost:   102,
		damage: 0,
		armor:  5,
	},
}

var rings = []Item{
	Item{
		name:   "none",
		cost:   0,
		damage: 0,
		armor:  0,
	},
	Item{
		name:   "Damage +1",
		cost:   25,
		damage: 1,
		armor:  0,
	},
	Item{
		name:   "Damage +2",
		cost:   50,
		damage: 2,
		armor:  0,
	},
	Item{
		name:   "Damage +3",
		cost:   100,
		damage: 3,
		armor:  0,
	},
	Item{
		name:   "Defense +1",
		cost:   20,
		damage: 0,
		armor:  1,
	},
	Item{
		name:   "Defense +2",
		cost:   40,
		damage: 0,
		armor:  2,
	},
	Item{
		name:   "Defense +3",
		cost:   80,
		damage: 0,
		armor:  3,
	},
}

func day21part1(selfHitpoints int, opponentHitPoints int, opponentDamage int, opponentArmor int) int {
	goldSpent := 0
	minGoldSpent := 99999999
	for i := 0; i < len(weapons); i++ {
		for j := 0; j < len(armor); j++ {
			for k := 0; k < len(rings); k++ {
				for l := 0; l < len(rings); l++ {
					if k == 0 || l != k {
						goldSpent = weapons[i].cost + armor[j].cost + rings[k].cost + rings[l].cost
						selfDamage := weapons[i].damage + rings[k].damage + rings[l].damage
						selfArmor := armor[j].armor + rings[k].armor + rings[l].armor
						currHitpoints := selfHitpoints
						currOpponentHitPoints := opponentHitPoints

						//fmt.Printf("Weapon: %s, Armor: %s, Ring1: %s, Ring2: %s\n", weapons[i].name, armor[j].name, rings[k].name, rings[l].name)

						for currHitpoints > 0 && currOpponentHitPoints > 0 {

							if selfDamage-opponentArmor > 0 {
								currOpponentHitPoints -= (selfDamage - opponentArmor)
							} else {
								currOpponentHitPoints--
							}

							if currOpponentHitPoints > 0 {
								if opponentDamage-selfArmor > 0 {
									currHitpoints -= (opponentDamage - selfArmor)
								} else {
									currHitpoints--
								}
							} else {
								break
							}

							//fmt.Printf("The player deals %d-%d=%d damage; the boss goes down to %d hit points\n", selfDamage, opponentArmor, selfDamage-opponentArmor, currOpponentHitPoints)
							//fmt.Printf("The boss deals %d-%d=%d damage; the player goes down to %d hit points\n\n", opponentDamage, selfArmor, opponentDamage-selfArmor, currHitpoints)
						}

						if currHitpoints > 0 {
							if goldSpent < minGoldSpent {
								minGoldSpent = goldSpent
							}
						}
					}
				}
			}
		}
	}
	return minGoldSpent
}

func day21part2(selfHitpoints int, opponentHitPoints int, opponentDamage int, opponentArmor int) int {
	goldSpent := 0
	maxGoldSpent := -1
	for i := len(weapons) - 1; i >= 0; i-- {
		for j := len(armor) - 1; j >= 0; j-- {
			for k := len(rings) - 1; k >= 0; k-- {
				for l := len(rings) - 1; l >= 0; l-- {
					if k == 0 || l != k {
						goldSpent = weapons[i].cost + armor[j].cost + rings[k].cost + rings[l].cost
						selfDamage := weapons[i].damage + rings[k].damage + rings[l].damage
						selfArmor := armor[j].armor + rings[k].armor + rings[l].armor
						currHitpoints := selfHitpoints
						currOpponentHitPoints := opponentHitPoints

						//fmt.Printf("Weapon: %s, Armor: %s, Ring1: %s, Ring2: %s, cost: %d\n", weapons[i].name, armor[j].name, rings[k].name, rings[l].name, goldSpent)

						for currHitpoints > 0 && currOpponentHitPoints > 0 {

							if selfDamage-opponentArmor > 0 {
								currOpponentHitPoints -= (selfDamage - opponentArmor)
							} else {
								currOpponentHitPoints--
							}

							if currOpponentHitPoints > 0 {
								if opponentDamage-selfArmor > 0 {
									currHitpoints -= (opponentDamage - selfArmor)
								} else {
									currHitpoints--
								}
							} else {
								break
							}

							//fmt.Printf("The player deals %d-%d=%d damage; the boss goes down to %d hit points\n", selfDamage, opponentArmor, selfDamage-opponentArmor, currOpponentHitPoints)
							//fmt.Printf("The boss deals %d-%d=%d damage; the player goes down to %d hit points\n\n", opponentDamage, selfArmor, opponentDamage-selfArmor, currHitpoints)
						}

						if currOpponentHitPoints > 0 && currHitpoints <= 0 {

							//fmt.Printf("  lost\n")
							if goldSpent > maxGoldSpent {
								maxGoldSpent = goldSpent
								// fmt.Printf("gold updated\n")
								// fmt.Printf("Weapon: %s, Armor: %s, Ring1: %s, Ring2: %s, cost: %d\n", weapons[i].name, armor[j].name, rings[k].name, rings[l].name, goldSpent)
								// fmt.Printf("Net boss attack: %d, Your net attack: %d\n\n", opponentDamage-selfArmor, selfDamage-opponentArmor)
							}
							//fmt.Printf("\n")
						} else {
							//fmt.Printf("  won\n\n")
						}
					}
				}
			}
		}
	}
	return maxGoldSpent
}
