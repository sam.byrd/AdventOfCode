package aoc

import (
	"fmt"
	"math"
)

// Day23Part1 is ...
func Day23Part1(data []string) (answer int) {
	a := 0
	b := 0

	for i := 0; i < len(data); {
		switch data[i][:3] {
		case "hlf":
			if data[i][4:5] == "a" {
				a = a / 2
			} else if data[i][4:5] == "b" {
				b = b / 2
			}
			i++

		case "tpl":
			if data[i][4:5] == "a" {
				a = a * 3
			} else if data[i][4:5] == "b" {
				b = b * 3
			}
			i++

		case "inc":
			if data[i][4:5] == "a" {
				a = a + 1
			} else if data[i][4:5] == "b" {
				b = b + 1
			}
			i++

		case "jmp":
			val := 0
			fmt.Sscanf(data[i], "jmp %d", &val)
			i += val

		case "jie":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jie %1s, %d", &reg, &val)
			if (reg == "a" && int(math.Mod(float64(a), float64(2))) == 0) ||
				(reg == "b" && int(math.Mod(float64(b), float64(2))) == 0) {
				i += val
			} else {
				i++
			}

		case "jio":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jio %1s, %d", &reg, &val)
			if (reg == "a" && a == 1) ||
				(reg == "b" && b == 1) {
				i += val
			} else {
				i++
			}

		}
	}
	return b
}

// Day23Part2 is ...
func Day23Part2(data []string) (answer int) {

	a := 1
	b := 0

	for i := 0; i < len(data); {
		switch data[i][:3] {
		case "hlf":
			if data[i][4:5] == "a" {
				a = a / 2
			} else if data[i][4:5] == "b" {
				b = b / 2
			}
			i++

		case "tpl":
			if data[i][4:5] == "a" {
				a = a * 3
			} else if data[i][4:5] == "b" {
				b = b * 3
			}
			i++

		case "inc":
			if data[i][4:5] == "a" {
				a = a + 1
			} else if data[i][4:5] == "b" {
				b = b + 1
			}
			i++

		case "jmp":
			val := 0
			fmt.Sscanf(data[i], "jmp %d", &val)
			i += val

		case "jie":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jie %1s, %d", &reg, &val)
			if (reg == "a" && int(math.Mod(float64(a), float64(2))) == 0) ||
				(reg == "b" && int(math.Mod(float64(b), float64(2))) == 0) {
				i += val
			} else {
				i++
			}

		case "jio":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jio %1s, %d", &reg, &val)
			if (reg == "a" && a == 1) ||
				(reg == "b" && b == 1) {
				i += val
			} else {
				i++
			}

		}
	}
	return b
}
