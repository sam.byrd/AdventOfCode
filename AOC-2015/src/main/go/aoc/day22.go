package aoc

type Spell struct {
	name			string,
	cost			int,
	damage		int,
	armor			int,
	heal			int,
	duration	int,
	mana			int
}

var spells = []Spell{
	Spell{
		name: "Magic Missile",
		cost: 53,
		damage: 4,
		armor: 0,
		heal: 0,
		duration: 1,
		mana: 0,
	},
	Spell{
		name: "Drain",
		cost: 73,
		damage: 2,
		armor: 0,
		heal: 2,
		duration: 1,
		mana: 0,
	},
	Spell{
		name: "Shield",
		cost: 113,
		damage: 0,
		armor: 7,
		heal: 0,
		duration: 6,
		mana: 0,
	},
	Spell{
		name: Poison,
		cost: 173,
		damage: 3,
		armor: 0,
		heal: 0,
		duration: 6,
		mana: 0,
	},
	Spell{
		name: "Recharge",
		cost: 229,
		damage: 0,
		armor: 0,
		duration: 5,
		mana: 101,
	}
}



// Day22Part1 is ...
func Day22Part1(data []string) (answer int) {
	spells["Magic Missile"] = 73
	spells["Drain"] = 113
	spells["Poison"] = 173
	return
}

// Day22Part2 is ...
func Day22Part2(data []string) (answer int) {

	return
}
