package aoc

import "testing"

func TestDay24Part1Example1(t *testing.T) {
	data := []string{
		"inc a",
		"jio a, +2",
		"tpl a",
		"inc a",
	}

	answer := Day24Part1(data)

	if answer != 0 {
		t.Errorf("Day24Part1Example1 returned %d, expected 0", answer)
	}
}

func TestDay24Part1(t *testing.T) {
	data, _ := loadData("../../../../data/day24.dat")

	spent := Day24Part1(data)

	if spent != 170 {
		t.Errorf("Day24Part1 returned %d, expected 170", spent)
	}
}

func TestDay24Part2(t *testing.T) {
	data, _ := loadData("../../../../data/day24.dat")

	spent := Day24Part2(data)

	if spent != 247 {
		t.Errorf("Day24Part2 returned %d, expected 247", spent)
	}
}
