package aoc

import (
	"fmt"
	"math"
)

type Sleigh struct {
	bag1 []int{}
	bag2 []int{}
	bag3 []int{}
	bag1QE int
}

func isBalanced(sleigh Sleigh) bool {
	b1 := getBagWeight(bag1)
	b2 := getBagWeight(bag2)
	if (b1 == b2) {
		b3 := getBagWeight(bag3)
		if (b1 == b3) {
			return true
		}
	}
	return false
}

func getBagWeight(weights []int) (answer int) {
	for _, w := weights {
		answer += w
	}
}

func getQE(weights []int) (qe int) {
	qe = 1
	for _, weight := range packages {
		qe *= weight
	}

	return qe
}

// Day24Part1 is ...
func Day24Part1(weights []string) (answer int) {

	sleighs := []Sleigh{}

	// start off with 3 sleigh configurations
	a := Sleigh()
	a.bag1 = append(a.bag1, weights[0])
	sleighs = append(sleighs, a)

	a := Sleigh()
	a.bag2 = append(a.bag2, weights[0])
	sleighs = append(sleighs, a)

	a := Sleigh()
	a.bag3 = append(a.bag3, weights[0])
	sleighs = append(sleighs, a)

	for i:=1; i < len(weights); i++ {

	}
}

// Day24Part2 is ...
func Day24Part2(data []string) (answer int) {

	a := 1
	b := 0

	for i := 0; i < len(data); {
		switch data[i][:3] {
		case "hlf":
			if data[i][4:5] == "a" {
				a = a / 2
			} else if data[i][4:5] == "b" {
				b = b / 2
			}
			i++

		case "tpl":
			if data[i][4:5] == "a" {
				a = a * 3
			} else if data[i][4:5] == "b" {
				b = b * 3
			}
			i++

		case "inc":
			if data[i][4:5] == "a" {
				a = a + 1
			} else if data[i][4:5] == "b" {
				b = b + 1
			}
			i++

		case "jmp":
			val := 0
			fmt.Sscanf(data[i], "jmp %d", &val)
			i += val

		case "jie":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jie %1s, %d", &reg, &val)
			if (reg == "a" && int(math.Mod(float64(a), float64(2))) == 0) ||
				(reg == "b" && int(math.Mod(float64(b), float64(2))) == 0) {
				i += val
			} else {
				i++
			}

		case "jio":
			val := 0
			reg := ""
			fmt.Sscanf(data[i], "jio %1s, %d", &reg, &val)
			if (reg == "a" && a == 1) ||
				(reg == "b" && b == 1) {
				i += val
			} else {
				i++
			}

		}
	}
	return b
}
