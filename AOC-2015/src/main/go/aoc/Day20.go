package aoc

import (
	"fmt"
	"math"
)

// Day20Part1 is ...
func Day20Part1(startingHouseNum int, target int) (houseNum int) {
	presents := 0

	for houseNum = startingHouseNum; presents < target; houseNum++ {
		presents = 0
		for elfNum := 1; presents < target && elfNum <= houseNum; elfNum++ {
			if houseNum%elfNum == 0 {
				presents += elfNum * 10
			}
		}
		if houseNum%1000 == 0 {
			fmt.Printf("House %d received %d presents\n", houseNum, presents)
		}
	}

	return houseNum - 1
}

// Day20Part2 is ...
func Day20Part2(startingHouseNum int, target int) (houseNum int) {
	presents := 0

	for houseNum = startingHouseNum; presents < target; houseNum++ {
		presents = 0
		elfCount := 0
		for elfNum := 0; presents < target && elfNum <= houseNum && elfCount < 50; elfNum++ {
			if houseNum%elfNum == 0 {
				elfCount++
				presents += elfNum * 11
			}
		}
		if houseNum%1000 == 0 {
			fmt.Printf("House %d received %d presents\n", houseNum, presents)
		}
	}

	return houseNum - 1
}

// Day20Part2_2 is ...
func Day20Part2_2(houseNum int) int {
	sum := 0
	d := int(math.Sqrt(float64(houseNum))) + 1
	for i := 1; i <= d; i++ {
		if houseNum%i == 0 {
			if i <= 50 {
				sum += houseNum / i
			}
			if houseNum/i <= 50 {
				sum += i
			}
		}

	}
	return sum * 11
}
