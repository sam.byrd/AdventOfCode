package aoc

import "testing"

func TestDay23Part1Example1(t *testing.T) {
	data := []string{
		"inc a",
		"jio a, +2",
		"tpl a",
		"inc a",
	}

	answer := Day23Part1(data)

	if answer != 0 {
		t.Errorf("Day23Part1Example1 returned %d, expected 0", answer)
	}
}

func TestDay23Part1(t *testing.T) {
	data, _ := loadData("../../../../data/day23.dat")

	spent := Day23Part1(data)

	if spent != 170 {
		t.Errorf("Day23Part1 returned %d, expected 170", spent)
	}
}

func TestDay23Part2(t *testing.T) {
	data, _ := loadData("../../../../data/day23.dat")

	spent := Day23Part2(data)

	if spent != 247 {
		t.Errorf("Day23Part2 returned %d, expected 247", spent)
	}
}
