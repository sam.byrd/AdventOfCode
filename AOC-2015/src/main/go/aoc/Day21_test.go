package aoc

import "testing"

func TestDay21Part1(t *testing.T) {
	spent := day21part1(100, 103, 9, 2)

	if spent != 121 {
		t.Errorf("Day21Part1 returned %d, expected 121", spent)
	}
}

func TestDay21Part2(t *testing.T) {
	spent := day21part2(100, 103, 9, 2)

	if spent != 201 {
		t.Errorf("Day21Part2 returned %d, expected 201", spent)
	}
}
