package Day17

import (
	"fmt"
	"testing"
)

func TestExample1(t *testing.T) {
	//var bottles []int := {47, 46, 36, 36, 32, 32, 31, 30, 28, 26, 19, 15, 11, 11, 5, 3, 3, 3, 1, 1}

	count := 0
	for a := 20; a >= 0; a -= 20 {
		for b := 15; b >= 0; b -= 15 {
			for c := 10; c >= 0; c -= 10 {
				for d := 5; d >= 0; d -= 5 {
					for e := 5; e >= 0; e -= 5 {
						if a+b+c+d+e == 25 {
							count++
						}
					}
				}
			}
		}
	}

	if count != 4 {
		t.Errorf("TestExample1 gave %d, expected 4", count)
	}
}
func TestExample2(t *testing.T) {

	count := 0
	containerCount := 0
	minContainers := 99
	for a := 20; a >= 0; a -= 20 {
		if a > 0 {
			containerCount += 1
		}
		for b := 15; b >= 0; b -= 15 {
			if b > 0 {
				containerCount += 1
			}
			for c := 10; c >= 0; c -= 10 {
				if c > 0 {
					containerCount += 1
				}
				for d := 5; d >= 0; d -= 5 {
					if d > 0 {
						containerCount += 1
					}
					for e := 5; e >= 0; e -= 5 {
						if e > 0 {
							containerCount += 1
						}
						if a+b+c+d+e == 25 {
							if containerCount < minContainers {
								fmt.Printf("New minimum %d %d %d %d %d - %d\n", a, b, c, d, e, containerCount)
								minContainers = containerCount
								count = 1
							} else if containerCount == minContainers {
								count++
							}
						}
						if e > 0 {
							containerCount -= 1
						}
					}
					if d > 0 {
						containerCount -= 1
					}
				}
				if c > 0 {
					containerCount -= 1
				}
			}
			if b > 0 {
				containerCount -= 1
			}
		}
		if a > 0 {
			containerCount -= 1
		}

		// now we have the minimum # of containers

	}

	if count != 3 {
		t.Errorf("TestExample2 gave %d, expected 3", count)
	}
}

func TestPart1(t *testing.T) {
	//{47, 46, 36, 36, 32, 32, 31, 30, 28, 26, 19, 15, 11, 11, 5, 3, 3, 3, 1, 1}
	count := 0
	for a := 47; a >= 0; a -= 47 {
		for b := 46; b >= 0; b -= 46 {
			for c := 36; c >= 0; c -= 36 {
				for d := 36; d >= 0; d -= 36 {
					for e := 32; e >= 0; e -= 32 {
						for f := 32; f >= 0; f -= 32 {
							for g := 31; g >= 0; g -= 31 {
								for h := 30; h >= 0; h -= 30 {
									for i := 28; i >= 0; i -= 28 {
										for j := 26; j >= 0; j -= 26 {
											for k := 19; k >= 0; k -= 19 {
												for l := 15; l >= 0; l -= 15 {
													for m := 11; m >= 0; m -= 11 {
														for n := 11; n >= 0; n -= 11 {
															for o := 5; o >= 0; o -= 5 {
																for p := 3; p >= 0; p -= 3 {
																	for q := 3; q >= 0; q -= 3 {
																		for r := 3; r >= 0; r -= 3 {
																			for s := 1; s >= 0; s -= 1 {
																				for t := 1; t >= 0; t -= 1 {
																					if a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+s+t == 150 {
																						count++
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if count != 1 {
		t.Errorf("TestPart1 gave %d, expected 1", count)
	}
}

func TestPart2(t *testing.T) {
	//{47, 46, 36, 36, 32, 32, 31, 30, 28, 26, 19, 15, 11, 11, 5, 3, 3, 3, 1, 1}
	count := 0
	containerCount := 0
	minContainers := 99
	for a := 47; a >= 0; a -= 47 {
		if a > 0 {
			containerCount += 1
		}
		for b := 46; b >= 0; b -= 46 {
			if b > 0 {
				containerCount += 1
			}
			for c := 36; c >= 0; c -= 36 {
				if c > 0 {
					containerCount += 1
				}
				for d := 36; d >= 0; d -= 36 {
					if d > 0 {
						containerCount += 1
					}
					for e := 32; e >= 0; e -= 32 {
						if e > 0 {
							containerCount += 1
						}
						for f := 32; f >= 0; f -= 32 {
							if f > 0 {
								containerCount += 1
							}
							for g := 31; g >= 0; g -= 31 {
								if g > 0 {
									containerCount += 1
								}
								for h := 30; h >= 0; h -= 30 {
									if h > 0 {
										containerCount += 1
									}
									for i := 28; i >= 0; i -= 28 {
										if i > 0 {
											containerCount += 1
										}
										for j := 26; j >= 0; j -= 26 {
											if j > 0 {
												containerCount += 1
											}
											for k := 19; k >= 0; k -= 19 {
												if k > 0 {
													containerCount += 1
												}
												for l := 15; l >= 0; l -= 15 {
													if l > 0 {
														containerCount += 1
													}
													for m := 11; m >= 0; m -= 11 {
														if m > 0 {
															containerCount += 1
														}
														for n := 11; n >= 0; n -= 11 {
															if n > 0 {
																containerCount += 1
															}
															for o := 5; o >= 0; o -= 5 {
																if o > 0 {
																	containerCount += 1
																}
																for p := 3; p >= 0; p -= 3 {
																	if p > 0 {
																		containerCount += 1
																	}
																	for q := 3; q >= 0; q -= 3 {
																		if q > 0 {
																			containerCount += 1
																		}
																		for r := 3; r >= 0; r -= 3 {
																			if r > 0 {
																				containerCount += 1
																			}
																			for s := 1; s >= 0; s -= 1 {
																				if s > 0 {
																					containerCount += 1
																				}
																				for t := 1; t >= 0; t -= 1 {
																					if t > 0 {
																						containerCount += 1
																					}
																					if a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p+q+r+s+t == 150 {
																						if containerCount < minContainers {
																							//fmt.Printf("New minimum %d %d %d %d %d - %d\n", a, b, c, d, e, containerCount)
																							minContainers = containerCount
																							count = 1
																						} else if containerCount == minContainers {
																							count++
																						}
																					}
																					if t > 0 {
																						containerCount -= 1
																					}
																				}
																				if s > 0 {
																					containerCount -= 1
																				}
																			}
																			if r > 0 {
																				containerCount -= 1
																			}
																		}
																		if q > 0 {
																			containerCount -= 1
																		}
																	}
																	if p > 0 {
																		containerCount -= 1
																	}
																}
																if o > 0 {
																	containerCount -= 1
																}
															}
															if n > 0 {
																containerCount -= 1
															}
														}
														if m > 0 {
															containerCount -= 1
														}
													}
													if l > 0 {
														containerCount -= 1
													}
												}
												if k > 0 {
													containerCount -= 1
												}
											}
											if j > 0 {
												containerCount -= 1
											}
										}
										if i > 0 {
											containerCount -= 1
										}
									}
									if h > 0 {
										containerCount -= 1
									}
								}
								if g > 0 {
									containerCount -= 1
								}
							}
							if f > 0 {
								containerCount -= 1
							}
						}
						if e > 0 {
							containerCount -= 1
						}
					}
					if d > 0 {
						containerCount -= 1
					}
				}
				if c > 0 {
					containerCount -= 1
				}
			}
			if b > 0 {
				containerCount -= 1
			}
		}
		if a > 0 {
			containerCount -= 1
		}
	}

	if count != 4 {
		t.Errorf("TestPart2 gave %d, expected 4", minContainers)
	}
}
