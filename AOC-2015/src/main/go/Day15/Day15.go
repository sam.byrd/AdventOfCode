package Day15

import "fmt"

type Ingredient struct {
	name       string
	capacity   int
	durability int
	flavor     int
	texture    int
	calories   int
}

func parseIngredient(input string) (ingredient Ingredient) {
	if _, err := fmt.Sscanf(input, "%s : capacity %d, durability %d, flavor %d, texture %d, calories %d", &ingredient.name, &ingredient.capacity, &ingredient.durability, &ingredient.flavor, &ingredient.texture, &ingredient.calories); err == nil {

	}
	return
}

func bake(data map[int]Ingredient, recipe map[int]int, count int, calorieTarget int) (score int) {
	capacity := 0
	durability := 0
	flavor := 0
	texture := 0
	calories := 0

	for i := 0; i < count; i++ {
		//fmt.Printf("%d ", recipe[i])
		capacity = capacity + (data[i].capacity * recipe[i])
		durability = durability + (data[i].durability * recipe[i])
		flavor = flavor + (data[i].flavor * recipe[i])
		texture = texture + (data[i].texture * recipe[i])
		calories = calories + (data[i].calories * recipe[i])
	}

	if capacity < 0 {
		capacity = 0
	}
	if durability < 0 {
		durability = 0
	}
	if flavor < 0 {
		flavor = 0
	}
	if texture < 0 {
		texture = 0
	}
	if calories < 0 {
		calories = 0
	}

	if calorieTarget == 0 || (calorieTarget > 0 && calories == calorieTarget) {
		score = capacity * durability * flavor * texture
	} else {
		score = 0
	}

	return
}

func solve1(data map[int]Ingredient, count int) (maxScore int) {

	maxScore = 0
	recipe := make(map[int]int)

	if count == 2 {
		for i := 100; i > 0; i-- {
			recipe[0] = i
			for j := 100 - i; j > 0; j-- {
				recipe[1] = j	
				if (i + j == 100) {
					score := bake(data, recipe, count, 0)
					if score > maxScore {
						maxScore = score
						//fmt.Printf("New high score: %d %d - %d\n", i, j, score)
					}
				}
			}
		}
	} else {
		for i := 100; i > 0; i-- {
			recipe[0] = i
			for j := 100 - i; j > 0; j-- {
				recipe[1] = j
				for k := 100 - i - j; k > 0; k-- {
					recipe[2] = k
					for l := 100 - i - j - k; l > 0; l-- {
						recipe[3] = l

						if (i + j + k + l == 100) {
							score := bake(data, recipe, count, 0)
							if score > maxScore {
								maxScore = score
								//fmt.Printf("New high score: %d %d %d %d - %d\n", i, j, k, l, score)
							}
						}
					}
				}
			}
		}
	}

	
	fmt.Printf("Max score: %d\n", maxScore)

	return
}

func solve2(data map[int]Ingredient, count int) (maxScore int) {

		maxScore = 0
		recipe := make(map[int]int)
	
		if count == 2 {
			for i := 100; i > 0; i-- {
				recipe[0] = i
				for j := 100 - i; j > 0; j-- {
					recipe[1] = j	
					if (i + j == 100) {
						score := bake(data, recipe, count, 500)
						if score > maxScore {
							maxScore = score
							//fmt.Printf("New high score: %d %d - %d\n", i, j, score)
						}
					}
				}
			}
		} else {
			for i := 100; i > 0; i-- {
				recipe[0] = i
				for j := 100 - i; j > 0; j-- {
					recipe[1] = j
					for k := 100 - i - j; k > 0; k-- {
						recipe[2] = k
						for l := 100 - i - j - k; l > 0; l-- {
							recipe[3] = l
	
							if (i + j + k + l == 100) {
								score := bake(data, recipe, count, 500)
								if score > maxScore {
									maxScore = score
									//fmt.Printf("New high score: %d %d %d %d - %d\n", i, j, k, l, score)
								}
							}
						}
					}
				}
			}
		}
	
		
		fmt.Printf("Max score: %d\n", maxScore)
	
		return
	}
