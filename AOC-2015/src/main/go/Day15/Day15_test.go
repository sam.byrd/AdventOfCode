package Day15

import (
	"bufio"
	"log"
	"os"
	"testing"
)

func loadData() (data map[int]Ingredient, count int) {
	file, err := os.Open("../../../../data/day15.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	count = 0
	data = make(map[int]Ingredient)
	// load the input array
	for scanner.Scan() {
		data[count] = parseIngredient(scanner.Text())
		count++
	}

	return
}

func TestParseIngredient(t *testing.T) {

	i := parseIngredient("Butterscotch : capacity -1, durability -2, flavor 6, texture 3, calories 8")

	if i.name != "Butterscotch" {
		t.Errorf("ParseButterscotch name: %s, want Butterscotch", i.name)
	}

	if i.capacity != -1 {
		t.Errorf("ParseIngredient capacity: %d, want -1", i.capacity)
	}

	if i.durability != -2 {
		t.Errorf("ParseIngredient durability: %d, want -2", i.durability)
	}

	if i.flavor != 6 {
		t.Errorf("ParseIngredient flavor: %d, want 6", i.texture)
	}

	if i.calories != 8 {
		t.Errorf("ParseIngredient calories: %d, want 8", i.flavor)
	}
}

func TestExample1(t *testing.T) {
	data := make(map[int]Ingredient)
	data[0] = parseIngredient("Butterscotch : capacity -1, durability -2, flavor 6, texture 3, calories 8")
	data[1] = parseIngredient("Cinnamon : capacity 2, durability 3, flavor -2, texture -1, calories 3")

	maxScore := solve1(data, 2)

	if maxScore != 62842880 {
		t.Errorf("TestExample1 maxScore was %d, want 62842880", maxScore)
	}
}

func TestExample1_bake(t *testing.T) {
	data := make(map[int]Ingredient)
	data[0] = parseIngredient("Butterscotch : capacity -1, durability -2, flavor 6, texture 3, calories 8")
	data[1] = parseIngredient("Cinnamon : capacity 2, durability 3, flavor -2, texture -1, calories 3")

	ingredients := make(map[int]int)
	ingredients[0] = 44
	ingredients[1] = 56

	score := bake(data, ingredients, 2, 0)

	if score != 62842880 {
		t.Errorf("TestExample1_bake score = %d, expected 62842880", score)
	}
}

func TestExample2_bake(t *testing.T) {
	data := make(map[int]Ingredient)
	data[0] = parseIngredient("Butterscotch : capacity -1, durability -2, flavor 6, texture 3, calories 8")
	data[1] = parseIngredient("Cinnamon : capacity 2, durability 3, flavor -2, texture -1, calories 3")

	ingredients := make(map[int]int)
	ingredients[0] = 40
	ingredients[1] = 60

	score := bake(data, ingredients, 2, 500)

	if score != 57600000 {
		t.Errorf("TestExample1_bake score = %d, expected 57600000", score)
	}
}

func TestPart1(t *testing.T) {
	data, count := loadData()
	maxScore := solve1(data, count)

	if maxScore != 18965440 {
		t.Errorf("Solve1 maxPoints was %d points, want 18965440", maxScore)
	}
}

func TestPart2(t *testing.T) {
	data, count := loadData()
	maxScore := solve2(data, count)

	if maxScore != 15862900 {
		t.Errorf("Solve2 maxPoints was %d points, want 15862900", maxScore)
	}
}
