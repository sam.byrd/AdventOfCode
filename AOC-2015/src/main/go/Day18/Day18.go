package aoc

import "fmt"

func countAround(yard [102][102]byte, y int, x int) (numLit int) {
	numLit = 0

	if yard[y-1][x-1] == '#' {
		numLit++
	}

	if yard[y-1][x] == '#' {
		numLit++
	}

	if yard[y-1][x+1] == '#' {
		numLit++
	}

	if yard[y][x-1] == '#' {
		numLit++
	}

	if yard[y][x+1] == '#' {
		numLit++
	}

	if yard[y+1][x-1] == '#' {
		numLit++
	}

	if yard[y+1][x] == '#' {
		numLit++
	}

	if yard[y+1][x+1] == '#' {
		numLit++
	}

	return
}

func parseLine(yard *[102][102]byte, rowNum int, rowData string) {
	for x := 0; x < len(rowData); x++ {
		yard[rowNum][x+1] = rowData[x]
	}
}

//Day18Solve1
func Day18Solve1(data [1000]string, count int, iterations int) (lightCount int) {
	var yard [102][102]byte
	var tempYard [102][102]byte
	lightCount = 0

	// init yard
	for y := 0; y < 102; y++ {
		for x := 0; x < 102; x++ {
			yard[y][x] = '.'
			tempYard[y][x] = '.'
		}
	}

	// load initial image
	for i := 0; i < count; i++ {
		parseLine(&yard, i+1, data[i])
	}

	for iteration := 0; iteration < iterations; iteration++ {
		for y := 1; y <= count; y++ {
			for x := 1; x <= count; x++ {

				// count lights that are on
				count := countAround(yard, y, x)
				//				fmt.Printf("%d %d - %c", y, x, count)
				if (yard[y][x] == '.' && count == 3) || (yard[y][x] == '#' && (count == 2 || count == 3)) {
					tempYard[y][x] = '#'
				} else {
					tempYard[y][x] = '.'
				}
			}
		}

		// copy back to yard
		for y := 1; y <= count; y++ {
			for x := 1; x <= count; x++ {
				yard = tempYard
			}
		}

		// print status
		// for y := 1; y <= count; y++ {
		// 	for x := 1; x <= count; x++ {
		// 		fmt.Printf("%c", yard[y][x])
		// 	}
		// 	fmt.Printf("\n")
		// }
		// fmt.Printf("\n")
	}

	// count # of lights on
	for y := 1; y <= count; y++ {
		for x := 1; x <= count; x++ {
			if yard[y][x] == '#' {
				lightCount++
			}
		}
	}

	return

}

func Day18Solve2(data [1000]string, count int, iterations int) (lightCount int) {
	var yard [102][102]byte
	var tempYard [102][102]byte
	lightCount = 0

	// init yard
	for y := 0; y < 102; y++ {
		for x := 0; x < 102; x++ {
			yard[y][x] = '.'
			tempYard[y][x] = '.'
		}
	}

	// load initial image
	for i := 0; i < count; i++ {
		parseLine(&yard, i+1, data[i])
	}

	yard[1][1] = '#'
	yard[1][count] = '#'
	yard[count][1] = '#'
	yard[count][count] = '#'

	// print status
	fmt.Printf("Starting Grid\n")
	for y := 1; y <= count; y++ {
		for x := 1; x <= count; x++ {
			fmt.Printf("%c", yard[y][x])
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")

	for iteration := 0; iteration < iterations; iteration++ {
		fmt.Printf("Iteration %d\n", iteration)
		for y := 1; y <= count; y++ {
			for x := 1; x <= count; x++ {

				// count lights that are on
				lightCount := countAround(yard, y, x)
				//				fmt.Printf("%d %d - %c", y, x, count)
				if (x == 1 && y == 1) || (x == 1 && y == count) || (x == count && y == 1) || (x == count && y == count) {
					fmt.Printf("Corner [%d][%d]\n", y, x)
					tempYard[y][x] = '#'
				} else if (yard[y][x] == '.' && lightCount == 3) || (yard[y][x] == '#' && (lightCount == 2 || lightCount == 3)) {
					tempYard[y][x] = '#'
				} else {
					tempYard[y][x] = '.'
				}
			}
		}

		// copy back to yard
		for y := 1; y <= count; y++ {
			for x := 1; x <= count; x++ {
				yard = tempYard
			}
		}

		// print status
		for y := 1; y <= count; y++ {
			for x := 1; x <= count; x++ {
				fmt.Printf("%c", yard[y][x])
			}
			fmt.Printf("\n")
		}
		fmt.Printf("\n")
	}

	// count # of lights on
	for y := 1; y <= count; y++ {
		for x := 1; x <= count; x++ {
			if yard[y][x] == '#' {
				lightCount++
			}
		}
	}

	return

}
