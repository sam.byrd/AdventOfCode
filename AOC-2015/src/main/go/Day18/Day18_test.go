package aoc

import (
	"testing"
)

func TestDay18Example1(t *testing.T) {
	var data [1000]string
	data[0] = ".#.#.#"
	data[1] = "...##."
	data[2] = "#....#"
	data[3] = "..#..."
	data[4] = "#.#..#"
	data[5] = "####.."

	lightCount := Day18Solve1(data, 6, 4)

	if lightCount != 4 {
		t.Errorf("Test18Example1 returned %d, expected 4", lightCount)
	}

}

func TestDayPart1(t *testing.T) {

	dataTemp, count := loadData("../../../../Data/Day18.dat")

	var data [1000]string

	for i := 0; i < count; i++ {
		data[i] = dataTemp[i]
	}

	lightCount := Day18Solve1(data, count, 100)

	if lightCount != 814 {
		t.Errorf("Test18Example1 returned %d, expected 814", lightCount)
	}
}

func TestDay18Part2(t *testing.T) {

	dataTemp, count := loadData("../../../../Data/Day18.dat")

	var data [1000]string

	for i := 0; i < count; i++ {
		data[i] = dataTemp[i]
	}

	lightCount := Day18Solve2(data, count, 100)

	if lightCount != 924 {
		t.Errorf("Test18Example1 returned %d, expected 924", lightCount)
	}
}
