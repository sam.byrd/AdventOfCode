package Day14

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"testing"
)

func TestParseReindeer(t *testing.T) {

	r := parseReindeer("Dancer can fly 27 km/s for 5 seconds, but then must rest for 132 seconds.")

	if r.name != "Dancer" {
		t.Errorf("ParseReindeer name: %s, want Dancer", r.name)
	}

	if r.rate != 27 {
		t.Errorf("ParseReindeer rate: %d, want 27", r.rate)
	}

	if r.maxtime != 5 {
		t.Errorf("ParseReindeer maxtime: %d, want 5", r.maxtime)
	}

	if r.resttime != 132 {
		t.Errorf("ParseReindeer resttime: %d, want 132", r.resttime)
	}

	fmt.Printf("test")
}

func loadData() (data map[int]Reindeer, count int) {
	file, err := os.Open("../../../../data/day14.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	count = 0
	data = make(map[int]Reindeer)
	// load the input array
	for scanner.Scan() {
		data[count] = parseReindeer(scanner.Text())
		count++
	}

	return
}

// For example, suppose you have the following Reindeer:
// Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
// Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
// After one second, Comet has gone 14 km, while Dancer has gone 16 km.
// After ten seconds, Comet has gone 140 km, while Dancer has gone 160 km.
// 	On the eleventh second, Comet begins resting (staying at 140 km), and Dancer continues on for a total distance of 176 km.
//	On the 12th second, both reindeer are resting. They continue to rest until the 138th second, when Comet flies for another ten seconds. On the 174th second, Dancer flies for another 11 seconds.

//In this example, after the 1000th second, both reindeer are resting, and Comet is in the lead at 1120 km (poor Dancer has only gotten 1056 km by that point). So, in this situation, Comet would win (if the race ended at 1000 seconds).

// After one second, Comet has gone 14 km, while Dancer has gone 16 km.
func TestSolve1_example1_step1(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve1(data, count, 1)

	if winner != 16 {
		t.Errorf("Solve1_example1_step1 winner was: %d, want 16", winner)
	}

	if data[0].currPos != 14 {
		t.Errorf("Solve1_example1_step1 Comet Pos was %d, wanted 14", data[0].currPos)
	}

	if data[1].currPos != 16 {
		t.Errorf("Solve1_example1_step1 Dancer Pos was %d, wanted 16", data[1].currPos)
	}
}

// After ten seconds, Comet has gone 140 km, while Dancer has gone 160 km.
func TestSolve1_example1_step2(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve1(data, count, 10)

	if winner != 160 {
		t.Errorf("Solve1_example1_step2 winner was: %d, want Dancer", winner)
	}

	if data[0].currPos != 140 {
		t.Errorf("Solve1_example1_step2 Comet Pos was %d, wanted 140", data[0].currPos)
	}

	if data[0].currState != "R" {
		t.Errorf("Solve1_example1_step2 Comet State was %s, wanted R", data[0].currState)
	}

	if data[1].currPos != 160 {
		t.Errorf("Solve1_example1_step2 Dancer Pos was %d, wanted 160", data[1].currPos)
	}

	if data[1].currState != "F" {
		t.Errorf("Solve1_example1_step2 Dancer State was %s, wanted F", data[1].currState)
	}
}

//	On the eleventh second, Comet begins resting (staying at 140 km), and Dancer continues on for a total distance of 176 km.
func TestSolve1_example1_step3(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve1(data, count, 11)

	if winner != 176 {
		t.Errorf("Solve1_example1_step3 winner was: %d, want Dancer", winner)
	}

	if data[0].currPos != 140 {
		t.Errorf("Solve1_example1_step3 Comet Pos was %d, wanted 140", data[0].currPos)
	}

	if data[0].currState != "R" {
		t.Errorf("Solve1_example1_step3 Comet State was %s, wanted R", data[0].currState)
	}

	if data[1].currPos != 176 {
		t.Errorf("Solve1_example1_step3 Dancer Pos was %d, wanted 176", data[1].currPos)
	}

	if data[1].currState != "R" {
		t.Errorf("Solve1_example1_step3 Dancer State was %s, wanted R", data[1].currState)
	}
}

// On the 12th second, both reindeer are resting.
func TestSolve1_example1_step4(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve1(data, count, 12)

	if winner != 176 {
		t.Errorf("Solve1_example1_step4 winner was: %d, want Dancer", winner)
	}

	if data[0].currPos != 140 {
		t.Errorf("Solve1_example1_step4 Comet Pos was %d, wanted 140", data[0].currPos)
	}

	if data[0].currState != "R" {
		t.Errorf("Solve1_example1_step4 Comet State was %s, wanted R", data[0].currState)
	}

	if data[1].currPos != 176 {
		t.Errorf("Solve1_example1_step4 Dancer Pos was %d, wanted 176", data[1].currPos)
	}

	if data[1].currState != "R" {
		t.Errorf("Solve1_example1_step4 Comet State was %s, wanted R", data[1].currState)
	}
}

// They continue to rest until the 138th second, when Comet flies for another ten seconds.
func TestSolve1_example1_step5(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve1(data, count, 1000)

	if winner != 1120 {
		t.Errorf("Solve1_example1_step5 winner was: %d, want Comet", winner)
	}

	if data[0].currPos != 1120 {
		t.Errorf("Solve1_example1_step5 Comet Pos was %d, wanted 1120", data[0].currPos)
	}

	if data[0].currState != "R" {
		t.Errorf("Solve1_example1_step5 Comet State was %s, wanted R", data[0].currState)
	}

	if data[1].currPos != 1056 {
		t.Errorf("Solve1_example1_step5 Dancer Pos was %d, wanted 1056", data[1].currPos)
	}

	if data[1].currState != "R" {
		t.Errorf("Solve1_example1_step5 Comet State was %s, wanted R", data[1].currState)
	}
}

func TestSolve1_contest(t *testing.T) {
	data, count := loadData()
	raceduration := int(2503)
	winner := solve1(data, count, raceduration)

	if winner != 5 {
		t.Errorf("Solve1 winner was: %d, want 2640", winner)
	}
}

// They continue to rest until the 138th second, when Comet flies for another ten seconds.
func TestSolve2_example1(t *testing.T) {
	count := 0
	data := make(map[int]Reindeer)

	data[0] = parseReindeer("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.")
	data[1] = parseReindeer("Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.")
	count = 2

	winner := solve2(data, count, 1000)

	if winner != 689 {
		t.Errorf("Solve2_example1 winner had %d points, want 689", winner)
	}
}

func TestSolve2_contest(t *testing.T) {
	data, count := loadData()
	raceduration := int(2503)
	winner := solve2(data, count, raceduration)

	if winner != 5 {
		t.Errorf("Solve2 winner has %d points, want 1102", winner)
	}
}
