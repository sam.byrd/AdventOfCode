package Day14

import (
	"fmt"
)

type Reindeer struct {
	name              string
	rate              int
	maxtime           int
	resttime          int
	currPos           int
	currState         string
	currStateDuration int
	points            int
}

func parseReindeer(input string) (reindeer Reindeer) {
	if _, err := fmt.Sscanf(input, "%s can fly %d km/s for %d seconds, but then must rest for %d seconds.", &reindeer.name, &reindeer.rate, &reindeer.maxtime, &reindeer.resttime); err == nil {
		reindeer.currPos = 0
		reindeer.currState = "F"
		reindeer.currStateDuration = 0
		reindeer.points = 0
		//fmt.Printf("Name: %s, rate: %d km/s, maxtime: %d sec, resttime: %d sec\n", reindeer.name, reindeer.rate, reindeer.maxtime, reindeer.resttime)
	}
	return
}

func solve1(data map[int]Reindeer, count int, raceDuration int) (iLongestDistance int) {
	// run simulation

	iLongestDistance = 0

	for iRaceTime := 1; iRaceTime <= raceDuration; iRaceTime++ {
		for iReindeerNumber := 0; iReindeerNumber < count; iReindeerNumber++ {
			reindeer := data[iReindeerNumber]
			if reindeer.currState == "F" {
				reindeer.currPos = reindeer.currPos + reindeer.rate
				reindeer.currStateDuration++
				if reindeer.currStateDuration == reindeer.maxtime {
					reindeer.currState = "R"
					reindeer.currStateDuration = 0
				}
			} else {
				reindeer.currStateDuration++
				if reindeer.currStateDuration == reindeer.resttime {
					reindeer.currState = "F"
					reindeer.currStateDuration = 0
				}
			}

			data[iReindeerNumber] = reindeer
			if data[iReindeerNumber].currPos > iLongestDistance {
				iLongestDistance = data[iReindeerNumber].currPos
				fmt.Printf("%s moved into the lead at %d sec with a distance of %d km\n", data[iReindeerNumber].name, iRaceTime, data[iReindeerNumber].currPos)
			}
		}
	}

	return
}

func solve2(data map[int]Reindeer, count int, raceDuration int) (iMaxPoints int) {
	// run simulation

	iLongestDistance := 0

	for iRaceTime := 1; iRaceTime <= raceDuration; iRaceTime++ {
		for iReindeerNumber := 0; iReindeerNumber < count; iReindeerNumber++ {
			reindeer := data[iReindeerNumber]
			if reindeer.currState == "F" {
				reindeer.currPos = reindeer.currPos + reindeer.rate
				reindeer.currStateDuration++
				if reindeer.currStateDuration == reindeer.maxtime {
					reindeer.currState = "R"
					reindeer.currStateDuration = 0
				}
			} else {
				reindeer.currStateDuration++
				if reindeer.currStateDuration == reindeer.resttime {
					reindeer.currState = "F"
					reindeer.currStateDuration = 0
				}
			}

			data[iReindeerNumber] = reindeer
			if data[iReindeerNumber].currPos > iLongestDistance {
				iLongestDistance = data[iReindeerNumber].currPos
				fmt.Printf("%s moved into the lead at %d sec with a distance of %d km\n", data[iReindeerNumber].name, iRaceTime, data[iReindeerNumber].currPos)
			}
		}

		// check for leader to grant points
		for iReindeerNumber := 0; iReindeerNumber < count; iReindeerNumber++ {
			reindeer := data[iReindeerNumber]
			if data[iReindeerNumber].currPos == iLongestDistance {
				fmt.Printf("Reindeer %d, points changed from %d to ", iReindeerNumber, reindeer.points)
				reindeer.points++
				fmt.Printf("%d\n", reindeer.points)
				data[iReindeerNumber] = reindeer
			}
		}
		//fmt.Printf("\n")
	}

	// find reindeer with most points
	iMaxPoints = 0

	for iReindeerNumber := 0; iReindeerNumber < count; iReindeerNumber++ {
		if data[iReindeerNumber].points > iMaxPoints {
			iMaxPoints = data[iReindeerNumber].points
		}
	}
	return
}
