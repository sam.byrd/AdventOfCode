package Day16

import (
	"bufio"
	"log"
	"os"
	"testing"
)

//children: 3
//cats: 7
//samoyeds: 2
//pomeranians: 3
//akitas: 0
//vizslas: 0
//goldfish: 5
//trees: 3
//cars: 2
//perfumes: 1

func TestParseAunt(t *testing.T) {
	mfcsamResult := MFCSAMResult{3, 7, 2, 3, 0, 0, 5, 3, 2, 1}

	auntNum := parseAunt("Sue 1 goldfish 9, cars 0, samoyeds 9", mfcsamResult)

	if auntNum != 62842880 {
		t.Errorf("TestParseAunt was %d, want 62842880", auntNum)
	}
}

func TestSolve1(t *testing.T) {
	mfcsamResult := MFCSAMResult{3, 7, 2, 3, 0, 0, 5, 3, 2, 1}

	file, err := os.Open("../../../../data/day16.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	auntNum := 0
	// load the input array
	for scanner.Scan() {
		auntNum = parseAunt(scanner.Text(), mfcsamResult)
		if auntNum > 0 {
			break
		}

	}

	if auntNum == 40 {
		t.Errorf("TestSolve1 aunt was %d, want 40", auntNum)
	}
}

func TestSolve2(t *testing.T) {
	mfcsamResult := MFCSAMResult{3, 7, 2, 3, 0, 0, 5, 3, 2, 1}

	file, err := os.Open("../../../../data/day16.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	auntNum := 0
	// load the input array
	for scanner.Scan() {
		auntNum = parseAunt2(scanner.Text(), mfcsamResult)
		if auntNum > 0 {
			break
		}

	}

	if auntNum == 0 {
		t.Errorf("TestSolve2 aunt was %d, want 5", auntNum)
	}
}
