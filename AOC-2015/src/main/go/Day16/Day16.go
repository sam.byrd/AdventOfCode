package Day16

import (
	"fmt"
)

type MFCSAMResult struct {
	children    int
	cats        int
	samoyeds    int
	pomeranians int
	akitas      int
	vizslas     int
	goldfish    int
	trees       int
	cars        int
	perfumes    int
}

func parseAunt(data string, mfcsamData MFCSAMResult) (auntNumber int) {
	//Sue 1: goldfish: 9, cars: 0, samoyeds: 9
	auntNumber = 0
	var propName [3]string
	var propValue [3]int

	// strip off Aunt #
	if _, err := fmt.Sscanf(data, "Sue %d %s %d, %s %d, %s %d", &auntNumber, &propName[0], &propValue[0], &propName[1], &propValue[1], &propName[2], &propValue[2]); err == nil {
		fmt.Printf("Aunt num: %d\n", auntNumber)

		for i := 0; i < 3; i++ {

			fmt.Printf("  Prop %s - %d  ", propName[i], propValue[i])
			if propName[i] == "children" && mfcsamData.children != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "cats" && mfcsamData.cats != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "samoyeds" && mfcsamData.samoyeds != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "pomeranians" && mfcsamData.pomeranians != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "akitas" && mfcsamData.akitas != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "vizslas" && mfcsamData.vizslas != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "goldfish" && mfcsamData.goldfish != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "trees" && mfcsamData.trees != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "cars" && mfcsamData.cars != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "perfumes" && mfcsamData.perfumes != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else {
				fmt.Printf("matched")
			}
			fmt.Printf("\n")
		}
		fmt.Printf("\n")
	} else {
		fmt.Printf("Error grabbing aunt number - %s", err)
	}

	return
}

func parseAunt2(data string, mfcsamData MFCSAMResult) (auntNumber int) {
	//Sue 1: goldfish: 9, cars: 0, samoyeds: 9
	auntNumber = 0
	var propName [3]string
	var propValue [3]int

	// strip off Aunt #
	if _, err := fmt.Sscanf(data, "Sue %d %s %d, %s %d, %s %d", &auntNumber, &propName[0], &propValue[0], &propName[1], &propValue[1], &propName[2], &propValue[2]); err == nil {
		fmt.Printf("Aunt num: %d\n", auntNumber)

		for i := 0; i < 3; i++ {

			fmt.Printf("  Prop %s - %d  ", propName[i], propValue[i])
			if propName[i] == "children" && propValue[i] != mfcsamData.children {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "cats" && propValue[i] <= mfcsamData.cats {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "samoyeds" && propValue[i] != mfcsamData.samoyeds {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "pomeranians" && propValue[i] >= mfcsamData.pomeranians {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "akitas" && propValue[i] != mfcsamData.akitas {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "vizslas" && propValue[i] != mfcsamData.vizslas {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "goldfish" && propValue[i] >= mfcsamData.goldfish {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "trees" && propValue[i] != mfcsamData.trees {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "cars" && propValue[i] != mfcsamData.cars {
				fmt.Printf("\n")
				return 0
			} else if propName[i] == "perfumes" && mfcsamData.perfumes != propValue[i] {
				fmt.Printf("\n")
				return 0
			} else {
				fmt.Printf("matched")
			}
			fmt.Printf("\n")
		}
		fmt.Printf("\n")
	} else {
		fmt.Printf("Error grabbing aunt number - %s", err)
	}

	return
}
