package day19

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

// Part1 is ...
func Part1(input [1000]string, count int) int {
	// grab the starting string
	molecule := input[count-1]

	fmt.Printf("Count: %d, Molecule: %s\n\n", count, molecule)

	part1 := ""
	part2 := ""
	moleculeList := make(map[string]bool)

	for i := 0; i < count-2; i++ {
		fmt.Printf("%s\n", input[i])

		// reset our starting molecule
		molecule = input[count-1]

		// splitup the substitution
		fmt.Sscanf(input[i], "%s => %s", &part1, &part2)

		index := strings.Index(molecule, part1)
		substCount := 0
		tempMolecule := molecule
		for index != -1 {
			newMolecule := strings.Replace(tempMolecule, part1, part2, 1)
			newMolecule = strings.Replace(newMolecule, "*", part1, substCount)
			fmt.Printf("  %s\n", newMolecule)
			moleculeList[newMolecule] = true
			substCount++
			tempMolecule = strings.Replace(molecule, part1, "*", substCount)
			index = strings.Index(tempMolecule, part1)
		}
	}

	return len(moleculeList)
}


var combinations []string
var replacements = make(map[string][]string)

// Part2 is ...
func Part2(input [1000]string, count int) (steps int) {
	// grab the starting string
	targetMolecule := input[count-1]

	var mol string
	var replace string

	fmt.Printf("Count: %d\nTargetMolecule: %s\n\n", count, targetMolecule)

	for i := 0; i < count-2; i++ {
		split := strings.Split(input[i], "=>")
		if len(split) > 1 {
			mol = strings.Trim(split[0], " ")
			replace = strings.Trim(split[1], " ")
			replacements[mol] = append(replacements[mol], replace)
		}
	}

	for k, v := range replacements {
		//Get all the indexes for a Key
		indexes := allIndiciesForString(k, targetMolecule)
		for _, i := range indexes {
			//Save the head up to the index
			head := targetMolecule[:i]
			//Save the tail from the index + lenght of the searched key
			tail := targetMolecule[i+len(k):]

			//Create a string for all the replacement possbilities
			for _, com := range v {
				newMol := head + com + tail
				if !arrayutils.ContainsString(combinations, newMol) {
					combinations = append(combinations, newMol)
				}
			}
		}
}




//init Loads in the strings from the input file
func init() {
	file, _ := os.Open("input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		var mol string
		var replace string
		split := strings.Split(line, "=>")
		if len(split) > 1 {
			mol = strings.Trim(split[0], " ")
			replace = strings.Trim(split[1], " ")
			replacements[mol] = append(replacements[mol], replace)
		}
	}
	fmt.Println(replacements)
}

//allIndiciesForString finds all the indexes for a given string
func allIndiciesForString(s, in string) (indicies []int) {
	index := strings.Index(in, s)
	offset := 0
	for index > -1 {
		indicies = append(indicies, index+offset)
		//Offset is there to save how long the string was before it was cut to tail
		offset += len(in[:index+len(s)])
		in = in[index+len(s):]
		index = strings.Index(in, s)
	}

	return
}