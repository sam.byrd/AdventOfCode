package day19

import (
	"bufio"
	"log"
	"os"
	"testing"
)

func loadData(filename string) (data []string, count int) {
	data = make([]string, 1000)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	count = 0

	// load the input array
	for scanner.Scan() {
		if count > 1000 {
			break
		}
		data[count] = scanner.Text()
		count++
	}

	return
}

func TestDay19Example1(t *testing.T) {

	var data [1000]string
	data[0] = "H => HO"
	data[1] = "H => OH"
	data[2] = "O => HH"
	data[3] = ""
	data[4] = "HOH"

	count := Part1(data, 5)

	if count == 0 {
		t.Errorf("TestDay19Example1 gave %d, expected 4", count)
	}
}

func TestDay19Example2(t *testing.T) {

	var data [1000]string
	data[0] = "H => HO"
	data[1] = "H => OH"
	data[2] = "O => HH"
	data[3] = "e => H"
	data[4] = "e => O"
	data[5] = ""
	data[6] = "HOH"

	count := Part2(data, 7)

	if count != 3 {
		t.Errorf("TestDay19Example2 gave %d, expected 3", count)
	}
}

func TestDay19Example3(t *testing.T) {

	var data [1000]string
	data[0] = "H => HO"
	data[1] = "H => OH"
	data[2] = "O => HH"
	data[3] = "e => H"
	data[4] = "e => O"
	data[5] = ""
	data[6] = "HOHOHO"

	count := Part2(data, 7)

	if count != 6 {
		t.Errorf("TestDay19Example3 gave %d, expected 6", count)
	}
}

func TestPart1(t *testing.T) {

	dataTemp, count := loadData("../../../../Data/Day19.dat")

	var data [1000]string

	for i := 0; i < count; i++ {
		data[i] = dataTemp[i]
	}

	moleculeCount := Part1(data, count)

	if moleculeCount != 509 {
		t.Errorf("Test19Part1 returned %d, expected 509", moleculeCount)
	}
}

func TestPart2(t *testing.T) {

	dataTemp, count := loadData("../../../../Data/Day19.dat")

	var data [1000]string

	for i := 0; i < count; i++ {
		data[i] = dataTemp[i]
	}

	moleculeCount := Part2(data, count)

	if moleculeCount != 509 {
		t.Errorf("Test19Part1 returned %d, expected 509", moleculeCount)
	}
}
