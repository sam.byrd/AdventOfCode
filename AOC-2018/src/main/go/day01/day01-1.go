// Advent of Code - 2018.01

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	fmt.Println("Day 01.1")
	currFreq := int64(0)
	changes := make(map[int]int64)
	maxCounter := int(0)
	frequencies := make(map[int64]bool)

	file, err := os.Open("../../../../data/day01.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// load the input array
	for scanner.Scan() {
		inFreqStr := scanner.Text()
		changes[maxCounter], err = strconv.ParseInt(inFreqStr, 10, 64)

		if err != nil {
			fmt.Printf("Error converting %s to int\n", inFreqStr)
		}
		currFreq += changes[maxCounter]
		maxCounter++

	}
	fmt.Printf("Part 1 ending freqency is: %d\n", currFreq)

	// now continue looping until a duplicate frequency is encountered
	currFreq = 0
	bDone := false
	counter := 0

	for bDone == false {
		counter = 0
		for counter < maxCounter && bDone == false {
			currFreq += changes[counter]
			if frequencies[currFreq] {
				bDone = true
				fmt.Printf("Part 2 ending frequency is: %d\n", currFreq)
			} else {
				frequencies[currFreq] = true
				counter++
			}
		}
	}
}
