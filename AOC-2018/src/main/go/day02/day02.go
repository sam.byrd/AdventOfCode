// Advent of Code - 2018.01

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	fmt.Println("Day 02")
	example1 := make(map[int]string)
	example1[0] = "abcdef"
	example1[1] = "bababc"
	example1[2] = "abbcde"
	example1[3] = "abcccd"
	example1[4] = "aabcdd"
	example1[5] = "abcdee"
	example1[6] = "ababab"
	test01(example1)

	example2 := make(map[int]string)
	example2[0] = "abcde"
	example2[1] = "fghij"
	example2[2] = "klmno"
	example2[3] = "pqrst"
	example2[4] = "fguij"
	example2[5] = "axcye"
	example2[6] = "wvxyz"
	test02(example2, 7)
	data, count := loadData()

	part1(data, count)
	part2(data, count)
}

func loadData() (input map[int]string, count int) {
	file, err := os.Open("../../../../data/day02.dat")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	count = 0
	input = make(map[int]string)
	// load the input array
	for scanner.Scan() {
		input[count] = scanner.Text()
		count++
	}

	return
}
func part1(data map[int]string, count int) {

	results2 := 0
	results3 := 0

	fmt.Println("Part 1")
	for i := 0; i < count; i++ {
		has2, has3 := getCounts(data[i])

		if has2 == true {
			results2++
		}

		if has3 == true {
			results3++
		}
	}

	fmt.Printf("2=%d 3=%d checksum=%d", results2, results3, results2*results3)
}

func part2(data map[int]string, count int) {
	fmt.Println("====================")
	fmt.Println("Part 2")
	fmt.Println("====================")

	prob2(data, count)

	fmt.Println("====================")
	fmt.Println("")
}

func getCounts(input string) (has2 bool, has3 bool) {
	has2 = false
	has3 = false

	counts := make(map[byte]int)
	counts['a'] = 0
	counts['b'] = 0
	counts['c'] = 0
	counts['d'] = 0
	counts['e'] = 0
	counts['f'] = 0
	counts['g'] = 0
	counts['h'] = 0
	counts['i'] = 0
	counts['j'] = 0
	counts['k'] = 0
	counts['l'] = 0
	counts['m'] = 0
	counts['n'] = 0
	counts['o'] = 0
	counts['p'] = 0
	counts['q'] = 0
	counts['r'] = 0
	counts['s'] = 0
	counts['t'] = 0
	counts['u'] = 0
	counts['v'] = 0
	counts['w'] = 0
	counts['x'] = 0
	counts['y'] = 0
	counts['z'] = 0

	for i := 0; i < len(input); i++ {
		counts[input[i]]++
	}

	if counts['a'] == 2 {
		has2 = true
	}
	if counts['b'] == 2 {
		has2 = true
	}
	if counts['c'] == 2 {
		has2 = true
	}
	if counts['d'] == 2 {
		has2 = true
	}
	if counts['e'] == 2 {
		has2 = true
	}
	if counts['f'] == 2 {
		has2 = true
	}
	if counts['g'] == 2 {
		has2 = true
	}
	if counts['h'] == 2 {
		has2 = true
	}
	if counts['i'] == 2 {
		has2 = true
	}
	if counts['j'] == 2 {
		has2 = true
	}
	if counts['k'] == 2 {
		has2 = true
	}
	if counts['l'] == 2 {
		has2 = true
	}
	if counts['m'] == 2 {
		has2 = true
	}
	if counts['n'] == 2 {
		has2 = true
	}
	if counts['o'] == 2 {
		has2 = true
	}
	if counts['p'] == 2 {
		has2 = true
	}
	if counts['q'] == 2 {
		has2 = true
	}
	if counts['r'] == 2 {
		has2 = true
	}
	if counts['s'] == 2 {
		has2 = true
	}
	if counts['t'] == 2 {
		has2 = true
	}
	if counts['u'] == 2 {
		has2 = true
	}
	if counts['v'] == 2 {
		has2 = true
	}
	if counts['w'] == 2 {
		has2 = true
	}
	if counts['x'] == 2 {
		has2 = true
	}
	if counts['y'] == 2 {
		has2 = true
	}
	if counts['z'] == 2 {
		has2 = true
	}

	if counts['a'] == 3 {
		has3 = true
	}
	if counts['b'] == 3 {
		has3 = true
	}
	if counts['c'] == 3 {
		has3 = true
	}
	if counts['d'] == 3 {
		has3 = true
	}
	if counts['e'] == 3 {
		has3 = true
	}
	if counts['f'] == 3 {
		has3 = true
	}
	if counts['g'] == 3 {
		has3 = true
	}
	if counts['h'] == 3 {
		has3 = true
	}
	if counts['i'] == 3 {
		has3 = true
	}
	if counts['j'] == 3 {
		has3 = true
	}
	if counts['k'] == 3 {
		has3 = true
	}
	if counts['l'] == 3 {
		has3 = true
	}
	if counts['m'] == 3 {
		has3 = true
	}
	if counts['n'] == 3 {
		has3 = true
	}
	if counts['o'] == 3 {
		has3 = true
	}
	if counts['p'] == 3 {
		has3 = true
	}
	if counts['q'] == 3 {
		has3 = true
	}
	if counts['r'] == 3 {
		has3 = true
	}
	if counts['s'] == 3 {
		has3 = true
	}
	if counts['t'] == 3 {
		has3 = true
	}
	if counts['u'] == 3 {
		has3 = true
	}
	if counts['v'] == 3 {
		has3 = true
	}
	if counts['w'] == 3 {
		has3 = true
	}
	if counts['x'] == 3 {
		has3 = true
	}
	if counts['y'] == 3 {
		has3 = true
	}
	if counts['z'] == 3 {
		has3 = true
	}

	return
}

func test01(data map[int]string) {
	fmt.Println("====================")
	fmt.Println("Test01")
	fmt.Println("====================")

	results2 := 0
	results3 := 0

	for i := 0; i < 7; i++ {
		has2, has3 := getCounts(data[i])
		fmt.Printf("%d] 2=%t 3=%t\n", i, has2, has3)
		if has2 == true {
			results2++
		}

		if has3 == true {
			results3++
		}
	}

	fmt.Printf("Final counts: 2=%d 3=%d\n", results2, results3)

	if results2 == 4 && results3 == 3 {
		fmt.Println("success")
	} else {
		fmt.Println("fail")
	}

	fmt.Println("====================")
	fmt.Println("")
}

func compare(boxID1 string, boxID2 string) (count int) {
	count = 0
	//fmt.Printf("%s\n%s\n\n", boxID1, boxID2)
	for i := 0; i < len(boxID1); i++ {
		//fmt.Printf("%q %q\n", boxID1[i], boxID2[i])
		if boxID1[i] != boxID2[i] {
			count++
		}
	}
	//fmt.Printf("Count %d\n\n", count)

	return
}

func test02(data map[int]string, count int) {
	fmt.Println("====================")
	fmt.Println("Test2")
	fmt.Println("====================")

	prob2(data, count)

	fmt.Println("====================")
	fmt.Println("")
}

func prob2(data map[int]string, count int) {
	// loop through all, comparing to each other one
	for i := 0; i < count-1; i++ {
		for j := i + 1; j < count; j++ {
			diffCount := compare(data[i], data[j])
			if diffCount == 1 {
				// print out same
				fmt.Printf("%s\n%s\n", data[i], data[j])
				for k := 0; k < len(data[i]); k++ {
					if data[i][k] == data[j][k] {
						fmt.Printf("%c", data[i][k])
					}
				}
				fmt.Printf("\n")
			}
		}
	}
}
